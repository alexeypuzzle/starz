﻿using UnityEngine;

public class Ball : MonoBehaviour {

    Rigidbody2D rb;
    public bool isADrop;
    public LayerMask BottomBorderLayerInt;
	public float Torque;
    int BallsLayerId;
    int numberOfSideWallsHitInARow;
    bool hasHitBottom;
    bool isFlying;
    Vector2 pos;
    Vector2 direction;
    float bottomYcol;
	bool isSpinning = false;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
		BallsLayerId = LayerMask.NameToLayer("Balls");
	}

    public void Shoot(Vector2 direction){
        iTween.Stop(gameObject);
		isADrop = false;
		rb.gravityScale = 0;
		rb.velocity = Vector2.zero;
		gameObject.layer = BallsLayerId;
        rb.isKinematic = false;
        rb.AddForce(direction);
        isFlying = true;
	    rb.AddTorque(Torque);
//	    isSpinning = true;
    }

    void OnFall(){
		rb.gravityScale = 0;
		rb.velocity = Vector2.zero;
		gameObject.layer = BallsLayerId;
	}

    public void OnJoinAfterDrop(){
        isADrop = false;
		rb.gravityScale = 0;
		rb.velocity = Vector2.zero;
		gameObject.layer = BallsLayerId;
    }

    public void SnapToFirstReturnedBall(){
        //Vector3 dest = new Vector3(Viker_Gameplay.instance.BallsPos.x, Viker_Gameplay.instance.BallsPos.y, 0f);
        if(isADrop){
            iTween.MoveTo(gameObject, iTween.Hash("position", Viker_Gameplay.instance.BallsPos, "time", 1f, "oncomplete", "OnJoinAfterDrop")); 
	        
        } else{
            iTween.MoveTo(gameObject, iTween.Hash("position", Viker_Gameplay.instance.BallsPos, "time", 1f));
		}
    }

	public void OnObjectHit()
	{
        SoundManager.instance.playBallpopInGame();
		numberOfSideWallsHitInARow = 0;
	}

    public void OnSideWallHit(){
        if((Time.time - Viker_Gameplay.instance.TimeAtRelease) > Viker_Gameplay.instance.secondsUntilShowingTurbo){
			numberOfSideWallsHitInARow++;
			if (numberOfSideWallsHitInARow > 3)
			{
				nudgeDown();
			}    
        }
    }

    void nudgeDown(){
        rb.AddForce(Vector2.down * 2, ForceMode2D.Force);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("BottomBorder")){
			if (isFlying || isADrop)
			{
				iTween.RotateTo(gameObject, iTween.Hash("rotation", Vector3.zero, "time", 0.2f));
//				isSpinning = false;
				rb.angularVelocity = 0f;
				rb.isKinematic = true;
				direction = rb.velocity;
				rb.velocity = Vector2.zero;
				isFlying = false;
				if (isADrop)
				{
					transform.position = new Vector3(transform.position.x, 0f, 0f);
					OnFall();
				}
				Viker_Gameplay.instance.OnBallReturned(transform.position, isADrop);
			}
        }
    }

	void Update()
	{
//		if (isSpinning)
//		{
//			transform.Rotate(new Vector3(0f,0f,1f), Time.deltaTime * DegreesToRotatePerSecond);
//		}
	}

    void OnCollisionEnter2D(Collision2D col)
	{
        if (col.collider.CompareTag("SideWall") && !isADrop)
		{
            OnSideWallHit();
        }  else {
            OnObjectHit();
        }
	}

    public void SetDropPhysics(){
        rb.isKinematic = false;
        rb.gravityScale = 1;
		//rb.velocity = Vector2.zero;
        isADrop = true;
	}

    public void SetBallPhysics(){
		rb.isKinematic = true;
		rb.gravityScale = 0;
		//rb.velocity = Vector2.zero;
        isADrop = false;
	}

    private void OnDisable()
    {
        iTween.Stop(gameObject);
    }
}
