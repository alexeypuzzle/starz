﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class Block : Gem {
    public int Value{ get { return _value; }}
	public Text ValueText;
    public BreakDifficulty BreakDifficulty;
    public GameObject ExplosionPrefab;
    public Color Color{ get { return color; }}
    //public bool IsDummy;
	int _startingValue;
    int _value;

    Color color;


    public void InitializeAsDummy(){
		color = GetComponentInChildren<Image>().color;
        //print("set the color of a block to " + color);
		gameObject.SetActive(false);
    }

    void OnEnable()
    {
  //      if (!GameStateController.instance.IsContinuingSession)
		//{
		//	SetInitialValue();
		//	color = GetComponentInChildren<Image>().color;
		//}
    }

    public void SetInitialValue(){
        float multiplier = 1.0f;
        switch (BreakDifficulty)
        {
            case BreakDifficulty.Easy:
                multiplier = 1.0f;
                break;
            case BreakDifficulty.Medium:
                multiplier = 2.0f;
				break;
            case BreakDifficulty.Hard:
                multiplier = 4.0f;
				break;
            default:
                break;
        }
        _startingValue = Mathf.RoundToInt(Viker_Gameplay.instance.BallCount * multiplier);
        _value = _startingValue;
        UpdateValueText();
	}

    public void UpdateValueText(){
        ValueText.text = _value.ToString();
    }

    public void SetValue(int NewValue){
        _value = NewValue;
        UpdateValueText();
    }

    public void SetColor(Color newColor){
        color = newColor;
        GetComponentInChildren<Image>().color = newColor;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.collider.CompareTag("Ball")){
            _value--;
            if(_value <= 0){
                Explode();
            } else{
               UpdateValueText();
            }
        }
    }

    public void Explode(){
        Grid.RemoveFromGrid(transform.position);
        GameObject explosionGO = Instantiate(ExplosionPrefab, transform.parent, false);
		explosionGO.transform.position = new Vector3(transform.position.x + 0.5f, transform.position.y + 0.5f, transform.position.z);
		var ps = explosionGO.GetComponent<ParticleSystem>().main;
		ps.startColor = color;
        gameObject.SetActive(false);
    }

    public blockData getSaveData(){
		blockData newBlockData = new blockData();
        if (name.Contains("Clone")){
            newBlockData.prefabName = Viker_Utils.getCleanCloneName(name);
        } else{
            newBlockData.prefabName = Viker_Utils.getCleanPrefabName(name);    
        }
        newBlockData.value = _value;
        newBlockData.color4 = new float4(color);
        newBlockData.pos = new float3(Viker_Utils.roundDownPos(transform.position));
        return newBlockData;
    }
}

public enum BreakDifficulty{
 Easy, Medium, Hard
}


[Serializable]
public class blockData
{
	public string prefabName;
	public int value;
	public float4 color4;
    public float3 pos;
	public bool isStar;
	public bool isDrop;
}

