﻿using UnityEngine;
using System;

public class BottomBorder : MonoBehaviour {
    public static event EventHandler OnAllBallsReturned;
    public static BottomBorder instance;
    public Vector3 posOfFirstBallReturned;
    int _numberOfBallsReturned;

    void Awake()
    {
        instance = this;
        //Viker_Gameplay.OnBallsReleased += ResetCounter;
    }

    void OnDisable()
    {
	}
}
