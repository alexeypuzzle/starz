﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossPromo : MonoBehaviour
{
    public void OnClosePressed(){
        Viker_UIController.instance.ShowCrossPromo(false);
    }

    public void OnCrossPromoClicked()
    {
#if UNITY_IOS
        Application.OpenURL(Viker_URLS.GamePromo);

#elif UNITY_ANDROID
        Application.OpenURL(Viker_URLS.AmazeballsAndroid);
#endif
		Viker_UIController.instance.ShowCrossPromo(false);
    }
}
