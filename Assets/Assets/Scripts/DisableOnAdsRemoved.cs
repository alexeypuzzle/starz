﻿using UnityEngine;
using UnityEngine.UI;

public class DisableOnAdsRemoved : MonoBehaviour {

    Button button;

    void Awake()
    {
        button = GetComponent<Button>();
    }

    void Start(){
        PurchaseController.OnAdsRemovePurchased += onAdsRemoved;
		if (PlayerPrefs.GetInt(Viker_prefsKeys.HasRemovedAds, 0) == 0)
		{
			setEnabled(true);
		}
		else
		{
			setEnabled(false);
		}
    }

    void setEnabled(bool isEnabled){
        button.interactable = isEnabled;
    }

    void onAdsRemoved(){
        setEnabled(false);
    }

    void OnDisable()
    {
        PurchaseController.OnAdsRemovePurchased -= onAdsRemoved;
    }

}
