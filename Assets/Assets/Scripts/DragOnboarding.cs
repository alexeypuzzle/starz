﻿using UnityEngine;

public class DragOnboarding : MonoBehaviour {

    public GameObject hand;

    void OnEnable()
    {
        iTween.MoveBy(hand, iTween.Hash("looptype", iTween.LoopType.loop, "time", 1.5f, "amount", new Vector3(0, -100f, 0f)));
    }

}
