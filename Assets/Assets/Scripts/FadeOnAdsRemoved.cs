﻿using UnityEngine;
using UnityEngine.UI;

public class FadeOnAdsRemoved : MonoBehaviour {

	Image image;
	private void Awake()
	{
		image = GetComponent<Image>();
	}

	void Start()
	{
		PurchaseController.OnAdsRemovePurchased += onAdsRemoved;
        if(PlayerPrefs.GetInt(Viker_prefsKeys.HasRemovedAds, 0) == 0){
            setEnabled(true);
        } else{
            setEnabled(false);
        }
	}

	void setEnabled(bool isEnabled)
	{
        if(isEnabled){
            image.color = new Color(image.color.r, image.color.g, image.color.b, 1f);
        } else{
            image.color = new Color(image.color.r, image.color.g, image.color.b, 0.3f);
        }
	}

	void onAdsRemoved()
	{
        setEnabled(false);
		//transform.GetChild(0).GetComponent<Image>();
	}

	private void OnDisable()
	{
		PurchaseController.OnAdsRemovePurchased -= onAdsRemoved;
	}
}
