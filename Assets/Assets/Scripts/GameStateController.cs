﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;
using System;

public class GameStateController : MonoBehaviour
{

    public static GameStateController instance;
    public Session session;
    public bool IsContinuingSession { get { return _isContinuingSession; } }
    public bool HasRemovedAds { get { return PlayerPrefs.GetInt(Viker_prefsKeys.HasRemovedAds, 0) == 1; } }
    bool _isContinuingSession;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        if (LoadSession())
        {
            _isContinuingSession = true;
            Viker_UIController.instance.LoadUI(Viker_UI.Gameplay);
        }
    }

    public void ClearSession()
    {
        session = null;
        _isContinuingSession = false;
        if (File.Exists(Application.persistentDataPath + "/game.state"))
        {
            File.Delete(Application.persistentDataPath + "/game.state");
        }
    }

    bool LoadSession()
    {
#if UNITY_EDITOR
        return false;
#else
        if (File.Exists(Application.persistentDataPath + "/game.state"))
        {
            BinaryFormatter binary = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/game.state", FileMode.Open);
            session = (Session)binary.Deserialize(file);
            file.Close();
            Debug.LogWarning("Session found!");
            return true;
        }
        else
        {
            Debug.LogWarning("No session found");
            return false;
            //session = new Session();
        }
#endif
    }

    public void SaveSession()
    {
        session = new Session();
        session.isFirstAttempt = Viker_Gameplay.instance.IsFirstAttempt;
		for (int i = 0; i <= Grid.width - 1; i++)
		{
			for (int j = 0; j <= Grid.height - 1; j++)
			{
                if(Grid.data[i,j] != null){
					if (Grid.data[i, j].CompareTag("Block"))
					{
						session.blockData.Add(Grid.data[i, j].GetComponent<Block>().getSaveData());
					}
					else if (Grid.data[i, j].CompareTag("Star"))
					{
						session.starData.Add(Grid.data[i, j].GetComponent<ShapeStar>().GetSaveData());
					}
                    else if (Grid.data[i, j].CompareTag("Powerup")){
                        session.powerUpData.Add(Grid.data[i, j].GetComponent<PowerUp>().GetSaveData());
                    }    
                }
			}
		}
        session.ballCount = Viker_Gameplay.instance.BallCount;
        session.starCount = Viker_Gameplay.instance.Score;
        session.ballsPos = new float3(Viker_Gameplay.instance.BallsPos);
		BinaryFormatter binary = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/game.state");
		binary.Serialize(file, session);
		file.Close();
    }

    void printState()
    {
        //print("array length: " + state.data);
        //foreach (var item in session.shapeData)
        //{
        //    if(item != null){
        //        print("block count: " + item.blockData.Count);
        //    }
        //}
    }

    public bool HasASavedSession(){
        return (session != null);
    }

    public void OnSessionRestored(){
        _isContinuingSession = false;
    }
}

[Serializable]
public class Session
{
    public float3 ballsPos;
    public int ballCount;
    public int starCount;
    public List<blockData> blockData;
    public List<PowerUpData> powerUpData;
    public List<StarData> starData;
    public bool isFirstAttempt;
    public Session(){
        powerUpData = new List<PowerUpData>();
        blockData = new List<blockData>();
        starData = new List<StarData>();
    }
}

