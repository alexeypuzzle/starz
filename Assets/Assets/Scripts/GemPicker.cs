﻿using UnityEngine;
using System.Collections.Generic;

public class GemPicker : MonoBehaviour {

    public List<Block> GemOptions;
    public List<Block> dummyGemInstances;

    public Block GetRandomBlock(){
        int randomBlock = (int)Random.Range(0f, GemOptions.Count - 1);
        return GemOptions[randomBlock];
    }

    //public int GetVariationForBlock(Block block){
    //    //return block.getRandomVariationIndex();
    //}
	
}

