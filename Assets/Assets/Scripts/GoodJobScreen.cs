﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GoodJobScreen : MonoBehaviour
{
    int waveNo = 0;
	public Image Emoji;
	public Sprite[] Emojis;
	public Text BestScore;
	public Text CurrentScore;
    public Text WaveCompleteText;
	public ParticleSystem Explosion;
	List<Sprite> _spritePool;
	Sprite _currentEmoji;

	void OnEnable()
	{
		CreateNewPool();
		_currentEmoji = Emojis[Random.Range(0, _spritePool.Count)];
		Emoji.sprite = _currentEmoji;
		BestScore.text = PlayerPrefs.GetInt(Viker_prefsKeys.HighScore).ToString();
		CurrentScore.text = Viker_Gameplay.instance.Score.ToString();
		Explosion.Play();
	}

	void CreateNewPool()
	{
		_spritePool = new List<Sprite>();
		for (int i = 0; i < Emojis.Length; i++)
		{
			if (_currentEmoji == null)
			{
				_spritePool.Add(Emojis[i]);
			}
			else
			{
				if (Emojis[i] != _currentEmoji)
				{
					_spritePool.Add(Emojis[i]);
				}
			}			
		}
	}

	public void OnKeepGoingPressed()
	{
		if (!GameStateController.instance.HasRemovedAds)
		{
			Viker_adsController.OnInterstitialClosed += OnInterstitialFinished;
			Viker_adsController.OnShowInterstitialFailed += OnInterstitialFinished;
			Viker_adsController.instance.ShowInterstitial();
		}
		else
		{
			Viker_Gameplay.instance.OnGoodJobKeepPlayingFinished();
//			gameObject.SetActive(false);
		}
	}

	void OnInterstitialFinished()
	{
		Viker_Gameplay.instance.OnGoodJobKeepPlayingFinished();
		Viker_adsController.OnInterstitialClosed -= OnInterstitialFinished;
		Viker_adsController.OnShowInterstitialFailed -= OnInterstitialFinished;
//		gameObject.SetActive(false);
	}
}
