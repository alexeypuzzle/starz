﻿
using UnityEngine;
using System.Collections.Generic;

public class Grid : MonoBehaviour
{
    // TODO move shapes down, not the individual blocks. OR use the shapes as stamps and tie the blocks together.
    public static int width = 7;
    public static int height = 30;
    public static GameObject[,] data = new GameObject[width, height + 1];
    public static GameObject[,] mirror = new GameObject[width, height + 1];

    //#if UNITY_EDITOR
    //    void OnGUI()
    //    {
    //		if (GUILayout.Button("save"))
    //		{
    //            GameStateController.instance.SaveSession();
    //		}

    //        if(GUILayout.Button("load")){
    //            LoadGridFromMemory();
    //        }
    //    }e
    //#endif

    //public static void UpdateMirror()
    //{
    //    for (int i = 0; i <= width - 1; i++)
    //    {
    //        for (int j = 0; j <= height - 1; j++)
    //        {
    //            if (data[i, j] != null && mirror[i, j] == null)
    //            {
    //                GameObject DebugSquare = Instantiate(Viker_Gameplay.instance.DebugSquarePrefab, new Vector3(i - 10f, j, 0f), Quaternion.identity) as GameObject;
    //                string valueString = i.ToString() + j.ToString();
    //                DebugSquare.GetComponent<Block>().SetValue(int.Parse(valueString));
    //                mirror[i, j] = DebugSquare;
    //            }
    //            else if (data[i, j] == null & mirror[i, j] != null)
    //            {
    //                Destroy(mirror[i, j]);
    //                mirror[i, j] = null;

    //            }
    //        }
    //    }
    //}
    public static void LoadGridFromMemory()
    {
        Clear();
        Session state = GameStateController.instance.session;
        Viker_Gameplay.instance.BallsPos = new Vector3(state.ballsPos.x, state.ballsPos.y, state.ballsPos.z);
        for (int i = 0; i < state.blockData.Count; i++)
        {
			blockData block = state.blockData[i];
            GameObject newBlockGO = Pooler.instance.GetNextObject(block.prefabName);
			newBlockGO.transform.position = new Vector3(block.pos.x, block.pos.y, block.pos.z);
            AddToGrid(newBlockGO);
			Block newBlock = newBlockGO.GetComponent<Block>();
            newBlock.SetValue(block.value);
			newBlock.SetColor(new Color(block.color4.r, block.color4.g, block.color4.b, block.color4.a));
            newBlockGO.SetActive(true);
        }
        for (int i = 0; i < state.powerUpData.Count; i++)
        {
            PowerUpData data = state.powerUpData[i];
            GameObject newPowerup = Pooler.instance.GetNextObject(Viker_blockNames.BallDrop);
            newPowerup.transform.position = new Vector2(data.pos.x, data.pos.y);
            AddToGrid(newPowerup);
            newPowerup.SetActive(true);
        }

        for (int i = 0; i < state.starData.Count; i++)
		{
            StarData data = state.starData[i];
            GameObject newStar = Pooler.instance.GetNextObject(Viker_blockNames.Star);
            newStar.transform.position = new Vector2(data.pos.x, data.pos.y);
		    if (data.Height != 0)
		    {
			    //print("grid loading a star WITH shapeheight.");
		        newStar.GetComponent<ShapeStar>().shapeHeight = data.Height;
		    }
		    else
		    {
		     //   print("grid loading a star without shapeheight.");
			    //print("rows above: " + data.rowsAboveStar);
			    //print("rows below: " + data.rowsBelowStar);
		        newStar.GetComponent<ShapeStar>().RowsAboveStar = data.rowsAboveStar;
		        newStar.GetComponent<ShapeStar>().RowsBelowStar = data.rowsBelowStar;
		    }
            AddToGrid(newStar);
            newStar.SetActive(true);
		}
    }

    void finishLoad()
    {
        Viker_Gameplay.instance.IsLoadedFromMemory = false;
    }

    public static bool isInsideBorders(Vector2 pos)
    {
        return ((int)pos.x >= 0 &&
            (int)pos.x < width &&
            (int)pos.y >= 0);
    }

    public static void Destroy(int x, int y)
    {
        if (isInsideBorders(new Vector2(x, y)))
        {
            if (data[x, y] != null && data[x, y].CompareTag("Block"))
            {
                data[x, y].SetActive(false);
                data[x, y] = null;
            }
        }
    }

	public static bool CanMoveAllDownByOne()
	{
		for (int i = 0; i <= width - 1; i++)
		{
			for (int j = 0; j <= height - 1; j++)
			{
				if (data[i, j] != null)
				{
					if (j - 1 <= 0)
					{
						if (data[i, j].CompareTag("Block") || data[i, j].CompareTag("Star"))
						{
							return false;
						}

					}
				}
			}
		}
		return true;
	}


    /// <summary>
    ///  returns true if moved all items down and none have transform.position.y == 0
    /// </summary>
    public static void MoveAllDownByOne()
    {
        //print("move all down by one");
        for (int i = 0; i <= width - 1; i++)
        {
            for (int j = 0; j <= height - 1; j++)
            {
                if (data[i, j] != null)
                {
                    if ((j - 1 <= 0) && data[i, j].CompareTag("Powerup"))
                    {
                        data[i, j].SetActive(false);
                        data[i, j] = null;

                    } else{
						GameObject GO = data[i, j];
						GO.transform.Translate(Vector2.down);
						data[i, j] = null;
						data[i, j - 1] = GO; 
                    }
                }
            }
        }
    }

    public static void DestroyBottomRows(int numberOfRows)
    {
        //print("Destroying from bottom for " + numberOfRows + " rows");
        for (int j = 0; j <= numberOfRows; j++)
        {
			for (int i = 0; i <= width - 1; i++)
			{
				if (data[i, j] != null)
				{
					if (data[i, j].CompareTag("Powerup"))
					{
						data[i, j].SetActive(false);
						data[i, j] = null;
					}
					else if (data[i, j].CompareTag("Star"))
					{
						data[i, j].SetActive(false);
                        data[i, j] = null;

						//data[i, j].GetComponent<ShapeStar>().Explode();
					}
					else if (data[i, j].CompareTag("Block"))
					{
						data[i, j].SetActive(false);
                        data[i, j] = null;
						//data[i, j].GetComponent<Block>().Explode();
					}
				}

			}
        }

    }
    public static void DestroyStarsFromTheBottom(int numberOfStarsToDestroy){
        int numberOfStarsDestroyed = 0;
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
                if(data[i,j] != null){
                    
                    if (data[i, j].CompareTag("Powerup"))
					{
						data[i, j].SetActive(false);
						data[i, j] = null;
					} else if (data[i, j].CompareTag("Star"))
					{
						data[i, j].GetComponent<ShapeStar>().Explode();
						numberOfStarsDestroyed++;
						if (numberOfStarsDestroyed == numberOfStarsToDestroy)
						{
							return;
						}
					}
                }
			}
		}
    }

    public static void Clear()
    {
        for (int i = 0; i <= width - 1; i++)
        {
            for (int j = 0; j <= height - 1; j++)
            {
                if (data[i, j] != null)
                {
                    data[i, j].SetActive(false);
                    data[i, j] = null;
                }
            }
        }
    }

    public static int GetNumberOfEmptyRowsFromTop()
    {
        int numberOfEmptyRows = 0;
        for (int j = (height - 1); j >= 0; j--)
        {
            for (int i = 0; i <= width - 1; i++)
            {
                if (data[i, j] != null && data[i, j].CompareTag("Block"))
                    return numberOfEmptyRows;
            }
            numberOfEmptyRows++;
        }
        return numberOfEmptyRows;
    }

    public static int GetHighestRowNumberWithABlock()
    {
        int rowNumber = 0;
        for (int j = (height - 1); j >= 0; j--)
        {
            for (int i = 0; i <= width - 1; i++)
            {
                if (data[i, j] != null && data[i, j].CompareTag("Block"))
                    return j;
            }
        }
        return rowNumber;
    }

    public static List<Vector2> GetEmptyBlocks()
    {
        List<Vector2> emptyBlocks = new List<Vector2>();
        for (int i = 0; i <= width - 1; i++)
        {
            for (int j = 0; j <= height - 1; j++)
            {
                if (data[i, j] == null)
                {
                    emptyBlocks.Add(new Vector2(i, j));
                }
            }
        }
        return emptyBlocks;
    }

    public static int GetLowestRowWithABlock()
    {
        int lowestRow = 0;
        for (int j = 0; j <= height - 1; j++)
        {
            for (int i = 0; i <= width - 1; i++)
            {
                if (data[i, j] != null && data[i, j].CompareTag("Block"))
                {
                    return lowestRow;
                }
            }
            lowestRow++;
        }
        return lowestRow;
    }



    public static bool IsBlockEmpty(Vector2 pos)
    {
        return (data[(int)pos.x, (int)pos.y] == null);
    }

    public static void MoveEveryThingDownBy(int numberOfRows)
    {
        for (int i = 0; i <= width - 1; i++)
        {
            for (int j = 0; j <= height - 1; j++)
            {
                if (data[i, j] != null)
                {
                    if (j - numberOfRows >= 0)
                    {
                        GameObject GO = data[i, j];
                        GO.transform.Translate(Vector2.down * numberOfRows);
                        data[i, j] = null;
                        data[i, j - numberOfRows] = GO;
                    }
                    else
                    {
                        if (data[i, j].CompareTag("Powerup"))
                        {
                            data[i, j].SetActive(false);
                            data[i, j] = null;
                        }
                    }
                }
            }
        }
    }

    public static int GetLowestRowWithStar()
    {
        for (int i = 0; i <= width - 1; i++)
        {
            for (int j = 0; j <= height - 1; j++)
            {
                if (data[i, j] == null || !data[i, j].CompareTag("Star")) continue;
                return j;
            }
        }
        return -1;
    }

    public static void AddToGrid(GameObject newObj)
    {
        Vector2 pos = Viker_Utils.roundDownPos(newObj.transform.position);
        data[(int)pos.x, (int)pos.y] = newObj;
    }

	public static void RemoveFromGrid(Vector2 pos)
	{
		Vector2 roundedPos = Viker_Utils.roundDownPos(pos);
		data[(int)roundedPos.x, (int)roundedPos.y] = null;
	}

    public static void ExplodeBlocksInRow(int row){
        if(row >= 0){
			for (int i = 0; i < width; i++)
			{
				if (data[i, row] != null && data[i, row].CompareTag("Block"))
				{
					data[i, row].GetComponent<Block>().Explode();
				}
			}    
        }
    }
	
	public static void ExplodeBottomTwoShapes()
	{
		int starsDestroyed = 0;
		for (int i = 0; i <= width - 1; i++)
		{
			for (int j = 0; j <= height - 1; j++)
			{
				if (data[i, j] != null && data[i, j].CompareTag("Star"))
				{
					data[i, j].GetComponent<ShapeStar>().Explode();
					starsDestroyed++;
					if (starsDestroyed == 2) break;
				}
			}
		}
	}
	
}
