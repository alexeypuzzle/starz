﻿using UnityEngine;

public class GuideLine : MonoBehaviour {
    
    void Update()
    {
        /* absolute angle */float angle = Vector3.Angle(new Vector3(0f, 1f, 0f), Viker_Gameplay.instance.ShootVector);
        if(Viker_Gameplay.instance.ShootVector.x >= 0){
            angle = -angle;
        }
        transform.eulerAngles = new Vector3(0f, 0f, angle);
    }

    void OnEnable()
    {
        transform.position = new Vector2(Viker_Gameplay.instance.BallsPos.x + 0.125f, Viker_Gameplay.instance.BallsPos.y + 0.125f);
    }

}
