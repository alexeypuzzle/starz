﻿using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour {

    public Toggle soundToggle;

    private void Start()
    {
        soundToggle.isOn = (PlayerPrefs.GetInt(Viker_prefsKeys.Sound, 1) == 1);
    }

    public void OnSoundToggled(bool isOn){
        SoundManager.instance.playBallpop();
        PlayerPrefs.SetInt(Viker_prefsKeys.Sound, (isOn) ? 1 : 0);
        soundToggle.isOn = isOn;
    }
    public void OnContinuePressed(){
        SoundManager.instance.playBallpop();
        Viker_UIController.instance.ShowPause(false);
        Viker_Gameplay.instance.OnPause();
    }

	public void OnQuitPressed()
	{
        SoundManager.instance.playBallpop();
        Viker_UIController.instance.LoadUI(Viker_UI.MainMenu);
        Viker_UIController.instance.ShowPause(false);
        GameStateController.instance.ClearSession();
        Pooler.instance.ResetPools();
	}

	public void OnRestartPressed()
	{
        SoundManager.instance.playBallpop();
        Viker_Gameplay.instance.ResetGame();
        Viker_UIController.instance.ShowPause(false);
	}
}
