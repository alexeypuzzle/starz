﻿using System.Collections.Generic;
using UnityEngine;

public class Pooler : MonoBehaviour {

    public static Pooler instance;
	public List<PoolSettings> PoolSettings;
    Dictionary<string, List<GameObject>> pools;
    public Transform PoolsFolder;

    private void Awake()
    {
        instance = this;
        pools = new Dictionary<string, List<GameObject>>();
    }

    void Start()
    {
        for (int i = 0; i < PoolSettings.Count; i++)
        {
            for (int j = 0; j < PoolSettings[i].poolSize; j++)
            {
                GameObject GO = Instantiate(PoolSettings[i].gameObject) as GameObject;
                GO.name = PoolSettings[i].name;
                if(!pools.ContainsKey(GO.name)){
                    pools[GO.name] = new List<GameObject>();
                }
                GO.SetActive(false);
                pools[GO.name].Add(GO);
                GO.transform.SetParent(PoolsFolder);
            }
        }
    }

    public GameObject GetNextObject(string prefabName){
        if (!pools.ContainsKey(prefabName)){
			Debug.LogError("couldn't find prefab " + prefabName);
			string availableNames = "";
			foreach (var key in pools.Keys)
			{
				availableNames += " " + key;
			}
			return null;
        }
             
        for (int i = 0; i < pools[prefabName].Count; i++)
        {
            if(!pools[prefabName][i].activeInHierarchy){
                return pools[prefabName][i];
            }
        }// will only hit here if no prefab found. Check to see if we should grow the pool.
        for (int i = 0; i < PoolSettings.Count; i++)
        {
            if (PoolSettings[i].name == prefabName){
                if(PoolSettings[i].willGrow){
                    print("hit WILL GROW for " + prefabName);
					GameObject GO = Instantiate(PoolSettings[i].gameObject);
                    GO.name = PoolSettings[i].name;
					GO.SetActive(false);
                    pools[GO.name].Add(GO);
                    GO.transform.SetParent(PoolsFolder);
                    return GO;
                }
            }
        }
        Debug.LogError("pooler fail");
        return null;
    }

    public void ResetPools(){
        foreach (var pool in pools.Values)
        {
            for (int i = 0; i < pool.Count; i++)
            {
                pool[i].SetActive(false);
            }
        }
    }
}


