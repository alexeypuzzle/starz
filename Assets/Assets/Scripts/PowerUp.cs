﻿using UnityEngine;
using System;

public class PowerUp : MonoBehaviour {


    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
		{
			if(!collision.gameObject.GetComponent<Ball>().isADrop){
                Drop();
            }
        }
    }

    public void Drop(){
        Grid.RemoveFromGrid(transform.position);
		Viker_Gameplay.instance.DropABall(transform.position);
        gameObject.SetActive(false);
    }

    public PowerUpData GetSaveData(){
        PowerUpData data = new PowerUpData();
        data.pos = new float3(transform.position);
        return data;
    }
}

[Serializable]
public class PowerUpData{
    public float3 pos;
}