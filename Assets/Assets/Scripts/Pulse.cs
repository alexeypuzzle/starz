﻿using UnityEngine;

public class Pulse : MonoBehaviour {

    void OnEnable()
    {
        iTween.ScaleTo(gameObject, iTween.Hash("scale", new Vector3(1.2f, 1.2f, 1.2f), "time", 0.5f, "looptype", iTween.LoopType.pingPong, "ignoretimescale", true));
    }

}
