﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class Shape : MonoBehaviour
{

    [SerializeField] int heightInBlocks;
    public int Height { get { return heightInBlocks; } }
    public List<Block> Blocks;
    public Transform Star;
    [SerializeField]int _rowsAboveStar;
    [SerializeField]int _rowsBelowStar;
    
    void DoShapeRowCalculations()
    {
        int highestRow = 0;
        int lowestRow = 100;
        for (int i = 0; i < Blocks.Count; i++)
        {
            if (Blocks[i].transform.position.y > highestRow)
            {
                highestRow = (int)Blocks[i].transform.position.y;
            }
            
            if (Blocks[i].transform.position.y < lowestRow)
            {
                lowestRow = (int)Blocks[i].transform.position.y;
            }
        }
        
        int starRow = (int)Star.position.y;
        _rowsAboveStar = highestRow - starRow;
        _rowsBelowStar = starRow - lowestRow;
        //print(string.Format(" {0} has {1} rows above and {2} rows below", name , _rowsAboveStar, _rowsBelowStar));
    }

    public void InitializeBlocks()
    {
        for (int i = 0; i < Blocks.Count; i++)
		{
            Blocks[i].InitializeAsDummy();
		    // So that the star knows how many rows to destroy.
		    DoShapeRowCalculations();
		}
    }

    public void SpawnBlocks()
    {
		if (!GameStateController.instance.IsContinuingSession)
		{
            for (int i = 0; i < Blocks.Count; i++)
			{
                Block block = Blocks[i];
				GameObject newBlock = Pooler.instance.GetNextObject(block.name);
                newBlock.transform.position = block.transform.position;
                newBlock.GetComponent<Block>().SetColor(block.Color);
                //print("setting the color of the block: " + block.Color);
                newBlock.GetComponent<Block>().BreakDifficulty = block.BreakDifficulty;
                newBlock.GetComponent<Block>().SetInitialValue();
                newBlock.SetActive(true);
				Grid.AddToGrid(newBlock);
			}
            GameObject newStar = Pooler.instance.GetNextObject(Star.name);
		    // stop setting shape height once shapes got asymmetrical.
            // newStar.GetComponent<ShapeStar>().shapeHeight = heightInBlocks;
		    //print("making a star with rows above: " + _rowsAboveStar);
		    //print("making a star with rows below: " + _rowsBelowStar);
		    newStar.GetComponent<ShapeStar>().RowsAboveStar = _rowsAboveStar;
		    newStar.GetComponent<ShapeStar>().RowsBelowStar = _rowsBelowStar;
            newStar.transform.position = Star.position;
            newStar.SetActive(true);
			Grid.AddToGrid(newStar);
			addPowerups();
		}
    }

    void addPowerups()
    {
        int roundedYPosOfShape = (int)Viker_Utils.roundDownPos(transform.position).y;
        for (int i = 0; i < Grid.width; i++)
        {
            for (int j = roundedYPosOfShape; j < (roundedYPosOfShape + heightInBlocks); j++)
            {
                Vector2 vectorToCheck = new Vector2(i, j); 
                if(Grid.IsBlockEmpty(vectorToCheck)){
					float rng = UnityEngine.Random.Range(0.0f, 1.0f);
					if (rng < Viker_Gameplay.instance.ExtraBallDropChance)
					{
                        GameObject newBallDrop = Pooler.instance.GetNextObject(Viker_Gameplay.instance.BallPowerUpPrefab.name);
                        newBallDrop.transform.position = vectorToCheck;
                        newBallDrop.SetActive(true);
                        Grid.AddToGrid(newBallDrop);
					}    
                }
            }
        }
    }

    //public void OnStarCollected()
    //{
    //    Viker_Gameplay.instance.OnStarCollected();
    //    selfDestruct();
    //}

//    void selfDestruct()
//    {
//        for (int i = 0; i < Blocks.Count; i++)
//        {
//            if (Blocks[i].gameObject.activeInHierarchy)
//            {
//                Blocks[i].Explode();
//            }
//        }
//        Invoke("DestroyShape", 0.5f);
//    }
//
//    void DestroyShape()
//    {
//        gameObject.SetActive(false);
//        //Destroy(gameObject);
//    }

    public ShapeData GetSaveData(){
        ShapeData shapeData = new ShapeData();
        //shapeData.PrefabName = Utils.getCleanCloneName(name);
        if(Star != null){
            shapeData.starPos = new float3(Star.position);    
        }
        for (int i = 0; i < Blocks.Count; i++)
        {
            if(Blocks[i] != null){
                shapeData.blockData.Add(Blocks[i].getSaveData());    
            }
        }
        return shapeData;
    }
}

[Serializable]
public class ShapeData{
    public List<blockData> blockData;
    public float3 starPos;
    //public string PrefabName;
    public ShapeData(){
        blockData = new List<blockData>();
    }
}

