﻿using UnityEngine;
using System;

public class ShapeStar : MonoBehaviour
{

	public int shapeHeight;
	public int RowsAboveStar;
	public int RowsBelowStar;
	int rowsAboveAndBelow; //used initially, when shapes were symetrical.

	// grid loads shapeheight.
	// shapestar calculates rowsabove and below.
	void OnEnable()
	{
		if (shapeHeight != 0)
		{
			print("got a star with a shapeheight.");
			rowsAboveAndBelow = (shapeHeight - 1) / 2;	
		}
	}

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Ball")){
            Viker_Gameplay.instance.OnStarCollected(transform.position);
			Explode();
        }
    }

    public void Explode()
    {
	    int minRow, maxRow;
		Grid.RemoveFromGrid(transform.position);
		Vector2 roundedPos = Viker_Utils.roundDownPos(transform.position);
	    // if shape has a height saved, it's an old symnmetrical shape.
	    if (shapeHeight != 0)
	    {
		    print("exploded the old way. with shape height.");
		    minRow = (int) roundedPos.y - rowsAboveAndBelow;
		    maxRow = (int) roundedPos.y + rowsAboveAndBelow;
	    }
	    else
	    {
		    print("exploded the new way. with rows below and above.");
		    minRow = (int) roundedPos.y - RowsBelowStar;
		    maxRow = (int) roundedPos.y + RowsAboveStar;
	    }
		    
		for (int i = minRow; i <= maxRow; i++)
		{
			Grid.ExplodeBlocksInRow(i);
		}
		SoundManager.instance.playStarCollected();
		gameObject.SetActive(false);
	}

    public StarData GetSaveData(){
        StarData newdata = new StarData();
	    newdata.rowsAboveStar = RowsAboveStar;
	    newdata.rowsBelowStar = RowsBelowStar;
	    // stopped saving shape height once shapes got asymmetrical.
//	    newdata.Height = shapeHeight;
        newdata.pos = new float3(transform.position);
        return newdata;
    }
}

[Serializable]
public class StarData
{
	public int Height;
	public int rowsAboveStar;
	public int rowsBelowStar;
    public float3 pos;
}
