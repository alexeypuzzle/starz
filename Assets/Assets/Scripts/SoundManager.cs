﻿using UnityEngine;

public class SoundManager : MonoBehaviour {

    [SerializeField] AudioClip ballPop;
    [SerializeField] AudioClip starCollected;
    public static SoundManager instance;
    public AudioSource collisionSounds;
    public AudioSource starSounds;
    public float collisionSoundCooldown = 0.05f;
    float timeAtLastSound;

    void Awake()
    {
        if(instance == null)
            instance = this;
    }

    public void playBallpop(){
        if(isSoundEnabled()){
            collisionSounds.PlayOneShot(ballPop);
        }
    }

	public void playBallpopInGame()
	{
		if (canPlaySoundInGame())
		{
			collisionSounds.PlayOneShot(ballPop);
			timeAtLastSound = Time.time;
		}
	}

	public void playStarCollected()
	{
        if (isSoundEnabled())
        {
            starSounds.PlayOneShot(starCollected);
        }
	}

	bool isSoundEnabled()
	{
		return (PlayerPrefs.GetInt(Viker_prefsKeys.Sound, 1) == 1);
	}

    bool canPlaySoundInGame(){
        return ( (Time.time - timeAtLastSound) > collisionSoundCooldown && isSoundEnabled() );
    }
}
