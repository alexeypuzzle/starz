﻿using System.Collections;
using UnityEngine;

public class SpinPowerup : MonoBehaviour {

	public float anglePerFrame;
    public bool clockwise = false;

    private void OnEnable()
    {
        StartCoroutine("spinMe");
    }

    IEnumerator spinMe()
	{
        if(clockwise){
            anglePerFrame = -anglePerFrame;
        }
		while (true)
		{
            transform.Rotate(new Vector3(0f, 0f, anglePerFrame));
			yield return new WaitForSeconds(0.01f);
		}

	}
}
