﻿using UnityEngine;
using UnityEngine.UI;

public class StarHighlighter : MonoBehaviour
{

	public Sprite FilledStar;
	public Sprite EmptyStar;
	public Image[] StarImages;

	void OnEnable()
	{
		for (int i = 0; i < StarImages.Length; i++)
		{
			StarImages[i].sprite = EmptyStar;
		}
	}

	public void OnStarClicked(int numberOfStars)
	{
		Application.OpenURL(Viker_URLS.StoreURL);
		for (int i = 0; i < numberOfStars; i++)
		{
			StarImages[i].sprite = FilledStar;
		}
	}
}
