﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Staranim : MonoBehaviour {

    void OnEnable()
    {
        bool goRight;
        float randomForce = Random.Range(1f, 1.5f);
        goRight = Random.Range(0, 2) == 0;
        Vector3 flyOffVector;
        flyOffVector = new Vector3(goRight ? randomForce : -randomForce, 0f, 0f);
        transform.DOMove(transform.position + flyOffVector, 0.25f).SetUpdate(true);
        transform.DOMoveY(transform.position.y + 0.5f, 0.125f).SetLoops(2, LoopType.Yoyo).SetUpdate(true).OnComplete(GoToDest);

    }

    void GoToDest()
    {
        transform.DOMove(new Vector3(3.5f, 15f), 0.2f).OnComplete(die);
    }

    void die()
    {
        Destroy(gameObject);
    }
}
