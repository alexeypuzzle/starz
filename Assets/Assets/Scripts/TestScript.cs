﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour {

    public GameObject star;
	
	// Update is called once per frame
	void Update () 
    {
        if(Input.anyKeyDown)
        {
            Instantiate(star, new Vector3(3.5f, 5f, 0f), Quaternion.identity);
        }
	}
}
