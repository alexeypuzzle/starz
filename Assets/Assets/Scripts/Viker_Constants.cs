﻿public class Viker_UI
{
    public static readonly string MainMenu = "MainMenu";
    public static readonly string Gameplay = "Gameplay";
    public static readonly string Settings = "Settings";
    public static readonly string DebugMenu = "DebugMenu";
}

public class Viker_URLS
{
    public static readonly string VikerWebsite = "http://viker.co.uk";
    public static readonly string AppleStore = "https://itunes.apple.com/app/id1257117355";
    public static readonly string GooglePlayStore = "https://play.google.com/store/apps/details?id=com.viker.ballzvsstarz";

    public static string GamePromo
    {
        get
        {
#if UNITY_IOS
            return "http://itunes.apple.com/app/id1308362262";
#elif UNITY_ANDROID
            return "https://play.google.com/store/apps/details?id=com.viker.emojiboom";
#endif
        }
    }

    public static readonly string AmazeballsAndroid = "https://play.google.com/store/apps/details?id=com.viker.amazeballs";
    public static string StoreURL
    {
        get
        {
#if UNITY_IOS
return AppleStore;
            #elif UNITY_ANDROID
            return GooglePlayStore;
#endif
        }
    }
}

public class Viker_prefsKeys
{
    public static readonly string DebugMode = "d";
    public static readonly string Music = "m";
    public static readonly string Sound = "s";
    public static readonly string HasRemovedAds = "a";
	public static readonly string BombCount = "b";
    public static readonly string HighScore = "h";
    public static readonly string playCount = "p";
    public static readonly string hasRated = "hr";
    
}

public class Viker_blockNames
{
	public static readonly string Square = "Square";
	public static readonly string BallDrop = "Powerup - ball";
	public static readonly string Triangle_up = "Triangle_up";
    public static readonly string Triangle_left = "Triangle_left";
    public static readonly string Triangle_down = "Triangle_down";
    public static readonly string Triangle_right = "Triangle_right";
    public static readonly string Hexagon = "Hexagon";
    public static readonly string Star = "Star";
}

