﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
//using UnityEngine.Advertisements;
using System;
using Facebook.Unity;

public class Viker_GameOverScreen : MonoBehaviour {

    public Text Score;
    public Text Highscore;
    public Text CountdownText;
    public int StartingTimeInSec;
    public GameObject AdPromptText;
    public GameObject ContinuePlayingOption;
    public GameObject PlayAgainOption;
    public GameObject Header;
    public Button ContinuePlayingButton;
//	public GameObject VideoInterstitial;
    bool watchingRewardedAd;
    bool watchingInterstitial;

	void OnEnable()
	{
        Viker_adsController.OnVideoAdFinished += OnVideoWatched;
		Viker_adsController.OnInterstitialClosed += OnInterstitialClosed;
		Viker_adsController.OnShowInterstitialFailed += OnInterstitialClosed;
		Viker_adsController.OnVideoAdAborted += OnVideoAborted;
		Viker_adsController.OnShowVideoAdFailed += OnVideoWatched;
		watchingRewardedAd = false;
		watchingInterstitial = false;
		Score.text = Viker_Gameplay.instance.Score.ToString();
		Highscore.text =  PlayerPrefs.GetInt(Viker_prefsKeys.HighScore).ToString();
		Header.SetActive(false);
		DoDeathChecks();
        //TODO Add Facebook event here
	}

	void DoDeathChecks()
	{
		if (Viker_Gameplay.instance.IsFirstAttempt)
		{
			ContinuePlayingOption.SetActive(true);
//			AdPromptText.SetActive(true);
			PlayAgainOption.SetActive(false);
			StartCoroutine("CountDown");
			if(!isOnline()){
				ContinuePlayingButton.interactable = false;
			}
		}
		else
		{
//			AdPromptText.SetActive(false);
			ContinuePlayingOption.SetActive(false);
			PlayAgainOption.SetActive(true);
		}	
	}
	
	public void OnCustomInterstitialClosed()
	{
		
	}
	
	void OnVideoAborted()
	{
		onHomePressed();
	}

	void OnVideoWatched()
	{
		Viker_Gameplay.instance.OnKeepPlaying();
		gameObject.SetActive(false);
	}

	public void LogGameFinishedEvent()
    {
        FB.LogAppEvent(
            "Game Finished"
        );
    }

	public void OnInterstitialClosed()
	{
		print("gameover on interstitial closed.");
		Viker_Gameplay.instance.PlayAgain();
		gameObject.SetActive(false);
	}

    public void onContinuePlayingPressed(){
        StopCoroutine("CountDown");
		watchingRewardedAd = true;
		if (PlayerPrefs.GetInt(Viker_prefsKeys.HasRemovedAds, 0) == 0)
		{
			#if UNITY_EDITOR
			print("an ad would show now. Continuing anyway.");
			OnVideoWatched();
			#endif
			Viker_adsController.instance.ShowRewardedVideo();
        } else{
            OnVideoWatched();
        }
	}

    public void onPlayAgainPressed(){
        watchingInterstitial = true;
		LogGameFinishedEvent();
        if (PlayerPrefs.GetInt(Viker_prefsKeys.HasRemovedAds, 0) == 0)
        { 
		#if UNITY_EDITOR
	        print("an ad would show now. Continuing anyway.");
	        OnInterstitialClosed();
		#else
			Viker_adsController.instance.ShowInterstitial();
#endif
	        
        } else{
            OnInterstitialClosed();
        }
    }

    public void onHomePressed()
	{
        GameStateController.instance.ClearSession();
		LogGameFinishedEvent();
        Viker_UIController.instance.LoadUI(Viker_UI.MainMenu);
	}

	IEnumerator CountDown()
	{
		for (int i = StartingTimeInSec; i > 0; i--)
		{
			CountdownText.text = i.ToString();
			yield return new WaitForSeconds(1);
		}
        // this coroutine is stopped by onContinuePlayingPressed. No need to check if watchingRewardedAd = true;
		if (PlayerPrefs.GetInt(Viker_prefsKeys.HasRemovedAds, 0) == 0)
		{
			if (!ShouldSeeCustomVideo())
			{
				Viker_adsController.instance.ShowInterstitial();	
			}
		}
		GameStateController.instance.ClearSession();
		Viker_UIController.instance.LoadUI(Viker_UI.MainMenu);
	}

    void OnDisable()
    {
        Header.SetActive(true);
        Viker_adsController.OnVideoAdFinished -= OnVideoWatched;
	    Viker_adsController.OnInterstitialClosed -= OnInterstitialClosed;
	    Viker_adsController.OnShowInterstitialFailed -= OnInterstitialClosed;
	    Viker_adsController.OnVideoAdAborted -= OnVideoAborted;
	    Viker_adsController.OnShowVideoAdFailed -= OnVideoWatched;
    }

	bool isOnline()
	{
		return Application.internetReachability != NetworkReachability.NotReachable;
	}

    void OnApplicationQuit()
    {
        // user shouldn't be able to restore a session if they quit the app from the gameover screen.
        GameStateController.instance.ClearSession();
    }

	bool ShouldSeeCustomVideo()
	{
		print("deathcount: " + PlayerPrefs.GetInt("deathcount", 0));
		return PlayerPrefs.GetInt("deathcount", 0) % 3 == 0;
	}
}
