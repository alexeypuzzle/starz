﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class Viker_Gameplay : MonoBehaviour
{
    #region Variables
    public static event EventHandler OnBallsReleased;
    public static Viker_Gameplay instance;
    public GameObject GameOverScreen;
	public GameObject Header;
    public GameObject GuideLine;
    public GameObject BallPrefab;
    public GameObject DebugSquarePrefab;
    public GameObject StarForPopup;
    public Text BallCountText;
    public Text BestScoreText;
    public List<Shape> Shapes;
    public List<Shape> EasyShapes;
    public Vector3 BallsPos;
    public GameObject BallPowerUpPrefab;
    public int PlayAreaWidthInBlocks;
    public bool IsFirstAttempt { get { return _isFirstAttempt; } }
    public float BallSpeed { get { return _ballSpeed; } }
    public int BallCountAtRelease { get { return _ballCountAtRelease; } }
    public int BallCount { get { return _balls.Count; } }
    public Vector3 ShootVector { get { return _shootVector; } }
    public Vector3 StartingMousePos { get { return _startingMousePos; } }
    public int Score { get { return starsCollected; } }
    public Transform TopOfSpawnZone;
    public int genericShapeHeightInBlocks;
    public float ExtraBallDropChance;
    public Text ScoreText;
    public GameObject dragOnboarding;
    public float secondsUntilShowingTurbo;
    public GameObject TurboButton;
    public Text TurboMultiplierText;
    public Transform DropsFolder;
    public bool IsLoadedFromMemory;
	public GameObject VideoInterstitial;
    public float TimeAtRelease { get { return timeAtBallRelease; } }
	public GameObject GoodJobScreen;
	int ShowAdEvery___Stars = 7;
    [SerializeField] int numberOfShapesToSpawnAtStart;
    [SerializeField] int rowsToJumpToAfterShapeDestroyed;
    [SerializeField] int rowsBetweenShapes = 1;
    Vector3 _startingMousePos;
    Vector3 _shootVector;
    int _ballCountAtRelease;
    int _numberOfBallsReturned;
    int _additionalStarsAdded;//stars added through the reward system between rounds
    int DropLayerId;
    bool ballsAreFiring;
    bool gameOver;
    bool _isFirstAttempt;
    bool isDragging;
    bool _isPlaying;
    bool validMouseStartPoint;
    List<Ball> _balls;
    List<Ball> _tempNewBalls;
    [SerializeField] float _ballSpeed;
    [SerializeField] float _ballIntervalInSeconds;
    int numberOfShapesSpawned;
    int numberOfEmptyStartingRows;
    int starsCollected;
    Vector2 spawnPoint;
    bool starCollectedThisTurn;
    int bestScore { get { return PlayerPrefs.GetInt(Viker_prefsKeys.HighScore); } }
    float timeAtBallRelease;
    bool firstBallReturned;
    Shape randomShape;
	bool _showGoodJobOnBallsReturned;
	bool _showRatingPopupAfterGoodJob;
	
    #endregion

    void Awake()
    {
        instance = this;
        _balls = new List<Ball>();
        _tempNewBalls = new List<Ball>();
        DropLayerId = LayerMask.NameToLayer("Drop");
        Application.targetFrameRate = 40;
    }

    void Start()
    {
        initializeShapeTemplates();
        if (GameStateController.instance.IsContinuingSession)
        {
            RestoreSession();
        }
        else
        {
            dragOnboarding.SetActive(true);
            _isFirstAttempt = true;
            ResetGame();
        }
        updateBestScore();
	    CheckForRequestAds();
    }

	void CheckForRequestAds()
	{
		if (PlayerPrefs.GetInt(Viker_prefsKeys.HasRemovedAds, 0) == 0)
		{
//			if (_isFirstAttempt)
//			{
//				Viker_adsController.instance.RequestVideo();
//			}
			Viker_adsController.instance.showBanner();
		}
		else
		{
			Viker_adsController.instance.hideBanner();
		}

	}

    void RestoreSession()
    {
		for (int i = 0; i < _balls.Count; i++)
		{
			_balls[i].gameObject.SetActive(false);
		}
		for (int i = 0; i < _tempNewBalls.Count; i++)
		{
			_tempNewBalls[i].gameObject.SetActive(false);
		}
        _balls.Clear();
        _tempNewBalls.Clear();
        updateBestScore();
        Session session = GameStateController.instance.session;
        _isFirstAttempt = session.isFirstAttempt;
        BallsPos = new Vector3(session.ballsPos.x, session.ballsPos.y, session.ballsPos.z);
        for (int i = 0; i < session.ballCount; i++)
        {
            GameObject newBallGO = Pooler.instance.GetNextObject(BallPrefab.name);
            newBallGO.transform.position = BallsPos;
            _balls.Add(newBallGO.GetComponent<Ball>());
            newBallGO.SetActive(true);
        }
        Grid.LoadGridFromMemory();
        starsCollected = session.starCount;
        ScoreText.text = starsCollected.ToString();
        spawnPoint = TopOfSpawnZone.position;
        numberOfShapesSpawned = 5; // needs to be more than 0
        gameOver = false;
        ballsAreFiring = false;
        starCollectedThisTurn = false;
        _isPlaying = true;
        GameOverScreen.SetActive(false);
        _numberOfBallsReturned = 0;
        showBallCountText(true);
        Invoke("enableShooting", 0.2f);
        Invoke("OnSessionRestored", 0.3f);
    }

    public void ResetGame()
    {
        for (int i = 0; i < _balls.Count; i++)
        {
            _balls[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < _tempNewBalls.Count; i++)
		{
			_tempNewBalls[i].gameObject.SetActive(false);
		}
	    GoodJobScreen.SetActive(false);
	    Header.SetActive(true);
        Time.timeScale = 0f;
        Pooler.instance.ResetPools();
        Grid.Clear();
		_balls.Clear();
		_tempNewBalls.Clear();
        starsCollected = 0;
        ScoreText.text = "0";
        updateBestScore();
        _numberOfBallsReturned = 0;
        numberOfShapesSpawned = 0;
        spawnPoint = TopOfSpawnZone.position;
		gameOver = false;
		ballsAreFiring = false;
		starCollectedThisTurn = false;
		_isPlaying = true;
        BallsPos = new Vector3(3f, 0f, -1f);
        if (Viker_UIController.instance.numberOfStartingStars != 0)
        {
            starsCollected = Viker_UIController.instance.numberOfStartingStars;
            ScoreText.text = starsCollected.ToString();
        }
        if (Viker_UIController.instance.numberOfStartingBalls != 0)
        {
            for (int i = 0; i < Viker_UIController.instance.numberOfStartingBalls; i++)
            {
                GameObject newBall = Pooler.instance.GetNextObject(BallPrefab.name);
                newBall.transform.position = BallsPos;
                newBall.GetComponent<Ball>().SetBallPhysics();
                _balls.Add(newBall.GetComponent<Ball>());
                newBall.SetActive(true);
            }
        }
        else
        {
            GameObject newBall = Pooler.instance.GetNextObject(BallPrefab.name);
            newBall.transform.position = BallsPos;
            newBall.GetComponent<Ball>().SetBallPhysics();
            _balls.Add(newBall.GetComponent<Ball>());
            newBall.SetActive(true);
        }
        NextRow();
        GameOverScreen.SetActive(false);
        Time.timeScale = 1.0f;
    }

    void initializeShapeTemplates(){
        for (int i = 0; i < Shapes.Count; i++)
        {
            Shapes[i].gameObject.SetActive(true);
            Shapes[i].InitializeBlocks();
        }
    }

    void spawnShapes()
    {
        // get a random shape and try to place it.
        //if (Grid.GetNumberOfEmptyRowsFromTop() >= getRandomShape().Height + 1){
            
        //}
        //int numberOfShapesToSpawn = Grid.GetNumberOfEmptyRowsFromTop() / genericShapeHeightInBlocks;
        if (numberOfShapesSpawned == 0)
        {
            spawnPoint = new Vector2(spawnPoint.x, 4f);

            for (int i = 0; i < numberOfShapesToSpawnAtStart; i++)
            {
                // for the first shape, pick an "easy" one.
				Shape randomShape = EasyShapes[Random.Range(0, EasyShapes.Count)];
                randomShape.transform.position = spawnPoint;
                randomShape.SpawnBlocks();
                numberOfShapesSpawned++;
                spawnPoint = new Vector2(spawnPoint.x, spawnPoint.y + (randomShape.Height) + 4f);
            }
        }
        else
        {
            while ( (getRandomShape().Height + rowsBetweenShapes) <= Grid.GetNumberOfEmptyRowsFromTop())
            {
                spawnPoint = new Vector2(spawnPoint.x, (Grid.GetHighestRowNumberWithABlock() + 1 + rowsBetweenShapes));
                randomShape.transform.position = spawnPoint;
                randomShape.SpawnBlocks();
                numberOfShapesSpawned++;
            }
        }
    }

    Shape getRandomShape()
    {
        randomShape = Shapes[Random.Range(0, Shapes.Count)];
        return randomShape;
    }

    void NextRow()
    {
        _numberOfBallsReturned = 0;
        Time.timeScale = 1.0f;
        for (int i = 0; i < _tempNewBalls.Count; i++)
        {
            _balls.Add(_tempNewBalls[i]);
        }
        _tempNewBalls.Clear();
//        Debug.Log("before collect balls");
        collectBalls();
//        Debug.Log("after collect balls");
        showBallCountText(true);
        Invoke("enableShooting", 0.2f);
        if (starCollectedThisTurn)
        {
            // if we can't do a big move down, move everything down by one.
            if (!tryBigMoveDown())
            {
                print("star collected but couldn't move stuff down.");
                moveEverythingDownByOne();
            }
            spawnShapes();
            starCollectedThisTurn = false;
        }
        else
        {
//            Debug.Log("before everything move down by one");
            moveEverythingDownByOne();
//            Debug.Log("after everything move down by one");
        }
        Invoke("save", 0.2f);
	}

    void save(){
        Debug.Log("before save");
		GameStateController.instance.SaveSession();
        Debug.Log("after save");
	}

    void moveEverythingDownByOne()
    {
        if (Grid.CanMoveAllDownByOne())
		{
            Grid.MoveAllDownByOne();
			spawnShapes();
		}
		else
		{
			Die();
		}
	}

    void enableShooting(){
        ballsAreFiring = false;
        firstBallReturned = false;
		SetTurbo(false);
	}

    bool tryBigMoveDown(){
		if (Grid.GetLowestRowWithABlock() - 1 > rowsToJumpToAfterShapeDestroyed)
		{
			int numberOfRowsToMoveEveryThingDownBy = Grid.GetLowestRowWithABlock() - 1 - rowsToJumpToAfterShapeDestroyed;
			Grid.MoveEveryThingDownBy(numberOfRowsToMoveEveryThingDownBy);
            return true;
		}
        return false;
    }

    public void OnStarCollected(Vector3 _position){
//        Debug.Log("3");
        Instantiate(StarForPopup, _position, Quaternion.identity);
        starCollectedThisTurn = true;
        starsCollected++;
        if(starsCollected > PlayerPrefs.GetInt(Viker_prefsKeys.HighScore,0)){
            PlayerPrefs.SetInt(Viker_prefsKeys.HighScore, starsCollected);
        }
        if ((starsCollected - _additionalStarsAdded) % ShowAdEvery___Stars == 0)
	    {
//            Debug.Log("4");
            if(starsCollected == 14)
            {
                ShowAdEvery___Stars = 5;
            }
		    _showGoodJobOnBallsReturned = true;
	    }
        updateScore();
	    if (starsCollected == 18)
	    {
		    if (PlayerPrefs.GetInt(Viker_prefsKeys.hasRated, 0) == 0)
		    {
			    _showRatingPopupAfterGoodJob = true;    
		    }
	    }
    }

    void updateScore(){
        ScoreText.text = starsCollected.ToString();
        if (starsCollected > bestScore){
            updateBestScore();
        }
    }

    public void AddStarsToScore(int _extraStars)
    {
        _additionalStarsAdded += _extraStars;
        starsCollected += _extraStars;
        updateScore();
    }


	public void Die()
	{
		IncrementDeathCount();
        Viker_adsController.instance.hideBanner();
		ballsAreFiring = false;
		gameOver = true;
		GameOverScreen.SetActive(true);
	}

	void IncrementDeathCount()
	{
		int deathCount = PlayerPrefs.GetInt("deathcount", 0);
		deathCount++;
		PlayerPrefs.SetInt("deathcount", deathCount);
	}

	bool hasClickedOnClickableArea()
	{
		Vector2 pointClicked = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		return pointClicked.y > 0.1f && pointClicked.y < 10.6f;
	}

    public void OnPause(){
		SoundManager.instance.playBallpop();
		if(_isPlaying){
            _isPlaying = false;
            Time.timeScale = 0f;
            Viker_UIController.instance.ShowPause(true);
        } else{
            _isPlaying = true;
            Time.timeScale = 1f;
            Viker_UIController.instance.ShowPause(false);
        }
    }

	void Update()
	{
		
#if UNITY_EDITOR
	if(Input.GetKeyDown(KeyCode.Space)) ShowGoodJobScreen();

#endif
		
        if (ballsAreFiring){
			if (Time.time - timeAtBallRelease > secondsUntilShowingTurbo)
			{
				SetTurbo(true);
			}
			return;
        }

		if (!ballsAreFiring && !gameOver && _isPlaying)
		{
			if (Input.GetMouseButtonDown(0))
			{
				if (hasClickedOnClickableArea())
				{
                    validMouseStartPoint = true;
                    dragOnboarding.SetActive(false);
					_startingMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				}
			}

			if (Input.GetMouseButton(0))
			{
                if  (validMouseStartPoint){
					Vector3 mousePosInWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    //print("mouse pos: " + mousePosInWorld);
                    //print("starting pos: " + _startingMousePos);
                    //print("y diff: " + (mousePosInWorld.y - _startingMousePos.y));
                    _shootVector = Vector3.Normalize(-(mousePosInWorld - _startingMousePos));
                    if(_shootVector.y < 0){
                        //print("shooting backwards");
                        isDragging = false;
                        GuideLine.SetActive(false);
                    } 
                    else if (_shootVector.y == 0.0f){
                        isDragging = false;
                        GuideLine.SetActive(false);
                    } 
                    else{
                        isDragging = true;
                        GuideLine.SetActive(true);
                    }
     //               if ((mousePosInWorld.y - _startingMousePos.y) < -0.25f){
					//	isDragging = true;	
					//}
                    //if(isDragging){
                        
                    //}

                    //else {
                        //_shootVector = new Vector3(_shootVector.x, 0.06f, 1.0f);
                        //isDragging = false;
                        //GuideLine.SetActive(false);
                    //}
                }
			}

			if (Input.GetMouseButtonUp(0))
			{
				if (isDragging)
				{
                    //print("shootverctor y: " + _shootVector.y);
                    if (_shootVector.y < 0.05f){
                        _shootVector = new Vector3(_shootVector.x, 0.05f, _shootVector.z);
                        //print("y is too small");
                        //GuideLine.SetActive(false);
                        //isDragging = false;
                        //return;
                    }
					GuideLine.SetActive(false);
					if (_shootVector.y >= 0)
					{
                        //print("shooting in " + _shootVector);
						StartCoroutine("ShootTheBalls");
						if (OnBallsReleased != null)
						{
							OnBallsReleased();
						}
					}
					isDragging = false;
                } else{
                    validMouseStartPoint = false;
                    GuideLine.SetActive(false);
                }
			}
		}
	}

	void updateBestScore()
	{
		BestScoreText.text = bestScore.ToString();
	}

	void OnSessionRestored()
	{
		GameStateController.instance.OnSessionRestored();
	}

#region Ball Behaviour

    public void SetNewBallsPos(Vector3 newpos ){
        //ballpo
    }

	IEnumerator ShootTheBalls()
	{
		if (!gameOver && !ballsAreFiring)
		{
            _numberOfBallsReturned = 0;
            Time.timeScale = 1.0f;
            firstBallReturned = false;
			timeAtBallRelease = Time.time;
			ballsAreFiring = true;
			_ballCountAtRelease = _balls.Count;
			int ballsFired = 0;
			Vector2 shootVector = new Vector2(_shootVector.x, _shootVector.y) * _ballSpeed;
			for (int i = 0; i < _balls.Count; i++)
			{
				if (_balls[i] != null)
				{
					_balls[i].Shoot(shootVector);
					ballsFired++;
					int ballsLeftToFire = _ballCountAtRelease - ballsFired;
					if (ballsLeftToFire != 0)
					{
						BallCountText.text = "x" + (BallCountAtRelease - ballsFired);
					}
					else
					{
						showBallCountText(false);
					}
					yield return new WaitForSeconds(_ballIntervalInSeconds);
				}
			}
			yield return null;
		}
	}

    void SetTurbo(bool isOn)
    {
        TurboButton.SetActive(isOn);
        if(!isOn){
            TurboMultiplierText.transform.parent.gameObject.SetActive(false);
        }
    }

	public void DropABall(Vector2 dropPoint)
	{
        Vector2 adjustedPos = new Vector2(dropPoint.x + 0.5f, dropPoint.y + 0.5f);
        GameObject newBallGO = Pooler.instance.GetNextObject(BallPrefab.name);
        newBallGO.transform.position = adjustedPos;
        //Instantiate(BallPrefab, adjustedPos, Quaternion.identity);
        //newBallGO.GetComponent<CircleCollider2D>().enabled = false;
        //newBallGO.transform.parent = transform;
		newBallGO.layer = DropLayerId;
		Ball newBall = newBallGO.GetComponent<Ball>();
        newBall.isADrop = true;
        newBall.SetDropPhysics();
		_tempNewBalls.Add(newBall);
		//newBallGO.GetComponent<Rigidbody2D>().gravityScale = 1;
        newBallGO.SetActive(true);
	}

	public void OnBallReturned(Vector2 pos, bool isADrop)
	{
//        Debug.Log("2");

        // if it's the first non-drop ball to return. set the new ballspos to that.
        if(!isADrop){
            _numberOfBallsReturned++;
            if (!firstBallReturned){
				BallsPos = pos;
				firstBallReturned = true;
            }
        }
        if (_numberOfBallsReturned == _ballCountAtRelease)
		{
            SetTurbo(false);
            Time.timeScale = 1.0f;
			if (_showGoodJobOnBallsReturned)
			{
				ShowGoodJobScreen();
			}
//            Debug.Log("before next row");
			NextRow();
//            Debug.Log("after next row");
		}
	}

    public void OnTurboPressed(){
        if (Time.timeScale < 8f){
			Time.timeScale = Time.timeScale * 2f;
			
			TurboMultiplierText.transform.parent.gameObject.SetActive(true);
			TurboMultiplierText.text = "x" + Time.timeScale;    
        }
    }
    
    void collectBalls()
    {
        if (_balls.Count > 1)
        {
            for (int i = 0; i < _balls.Count; i++)
            {
                _balls[i].SnapToFirstReturnedBall();
            }
        }
        BallCountText.gameObject.SetActive(true);
        BallCountText.text = "x" + _balls.Count;
    }

    void showBallCountText(bool isShowing)
    {
        BallCountText.text = "x" + _balls.Count;
        if (isShowing)
        {
            BallCountText.transform.parent.position = BallsPos;
            BallCountText.gameObject.SetActive(true);
        }
        else
        {
            BallCountText.gameObject.SetActive(false);
        }

    }

    public void addballs(int _amount)
    {
        for (int i = 0; i < _amount; i++)
        {
            GameObject newBall = Pooler.instance.GetNextObject(BallPrefab.name);
            newBall.transform.position = BallsPos;
            newBall.GetComponent<Ball>().SetBallPhysics();
            _balls.Add(newBall.GetComponent<Ball>());
            newBall.SetActive(true);
            showBallCountText(true);
        }
    }
#endregion

#region UI
	public void OnKeepPlaying()
	{
        GuideLine.SetActive(false);
		Invoke("ContinuePlayingSequence", 0.1f);
	}

	public void OnGoodJobKeepPlayingFinished()
	{
		GoodJobScreen.SetActive(false);
		Header.SetActive(true);
		if (_showRatingPopupAfterGoodJob)
		{
			Viker_UIController.instance.ShowRatingPopup();
			_showRatingPopupAfterGoodJob = false;  
		}
		Invoke("enableShooting", 0.2f);
		CheckForRequestAds();
		_isPlaying = true;
	}

	void ContinuePlayingSequence()
	{
        Time.timeScale = 1.0f;
		_isFirstAttempt = false;
		
		Grid.ExplodeBottomTwoShapes();
		
		int lowestStarRow = Grid.GetLowestRowWithStar();
//		
		int rowsToRemove = lowestStarRow - 10;
//		
		if (rowsToRemove > 0)
		{
			Grid.MoveEveryThingDownBy(rowsToRemove);
		}
		// get the row with the lowest star.
		// get the difference between the row with the lowest star and 5 (number of rows to clear)
		// move everything down by that difference if it's positive.
		GameOverScreen.SetActive(false);
		Header.SetActive(true);
		ballsAreFiring = false;
		gameOver = false;
        Invoke("enableShooting", 0.2f);
		CheckForRequestAds();
	}

	public void PlayAgain()
	{
		Viker_UIController.instance.numberOfStartingStars = 0;
		Viker_UIController.instance.numberOfStartingBalls = 0;
		_isFirstAttempt = true;
		Invoke("ResetGame", 0.1f);
		Invoke("CheckForRequestAds", 0.5f);
	}

	public void onBackButtonPressed()
	{
		Viker_UIController.instance.GoBack();
	}

	public void onSettingsButtonPressed()
	{
        SoundManager.instance.playBallpop();
		Viker_UIController.instance.LoadUI(Viker_UI.Settings);
	}
	#endregion

	public void ShowGoodJobScreen()
	{
		Header.SetActive(false);
		_isPlaying = false;
		GoodJobScreen.SetActive(true);
		_showGoodJobOnBallsReturned = false;
	}

}

public delegate void EventHandler();