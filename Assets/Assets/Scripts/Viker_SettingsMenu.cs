﻿using UnityEngine;
using UnityEngine.UI;

public class Viker_SettingsMenu : MonoBehaviour {

    public Toggle MusicToggle;
    public Toggle SoundToggle;

    private void Start()
    {
        MusicToggle.isOn = (PlayerPrefs.GetInt(Viker_prefsKeys.Music, 0) == 1);
        SoundToggle.isOn = (PlayerPrefs.GetInt(Viker_prefsKeys.Sound, 1) == 1);
    }


    public void OnMusicToggled(bool isOn){
        SoundManager.instance.playBallpop();
        PlayerPrefs.SetInt(Viker_prefsKeys.Music, (isOn) ? 1 : 0);
    }

	public void OnSoundToggled(bool isOn)
	{
        SoundManager.instance.playBallpop();
        PlayerPrefs.SetInt(Viker_prefsKeys.Sound, (isOn) ? 1 : 0);
	}

	public void OnBackButtonPressed()
	{
        SoundManager.instance.playBallpop();
		Viker_UIController.instance.GoBack();
	}

    public void OnDebugMenuPressed(){
        SoundManager.instance.playBallpop();
        Viker_UIController.instance.LoadUI(Viker_UI.DebugMenu);
    }
}
