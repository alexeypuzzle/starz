﻿using System.Collections.Generic;
using UnityEngine;

public class Viker_UIController : MonoBehaviour {

	public static Viker_UIController instance;
	public GameObject activeUI;
	public GameObject activeOverlay;
    public GameObject pause;
    public GameObject crossPromo;
    public GameObject homepageSettings;
	public GameObject RatingPopup;
	public Camera MainCam;
	public int NumberOfGameLoads{get { return PlayerPrefs.GetInt("GameLoadCount", 0); }}
//	public GameObject Carousel;
    public int numberOfStartingBalls;
    public int numberOfStartingStars;
	string cloneString = "(Clone)";
	Stack<string> screenHistory;

	void Awake()
	{
		Application.targetFrameRate = 60;
		instance = this;
		screenHistory = new Stack<string>();
	}

	void Start()
	{
		int numberOfGameLoads = PlayerPrefs.GetInt("GameLoadCount", 0);
		numberOfGameLoads++;
		PlayerPrefs.SetInt("GameLoadCount", numberOfGameLoads);
		GameObject debugScene = GameObject.FindGameObjectWithTag("UI");
		if (debugScene != null)
		{
			activeUI = debugScene;
		}
		else
		{
            print("no debug scene found.");
            LoadUI(Viker_UI.MainMenu);
		}
	}

    public void ShowPause(bool isShowing){
        pause.SetActive(isShowing);
    }

	public void ShowHomepageSettings(bool isShowing)
	{
		homepageSettings.SetActive(isShowing);
	}

    public void ShowCrossPromo(bool isShowing){
        crossPromo.SetActive(isShowing);
    }

	public void ShowRatingPopup()
	{
		RatingPopup.SetActive(true);
	}
	
	public void LoadUI(string newUI, bool clearHistory = false)
	{
		if (activeOverlay != null)
		{
			Destroy(activeOverlay);
		}
		
//		if (activeUI != null &&  activeUI.name == Viker_UI.Gameplay && newUI == Viker_UI.MainMenu)
//		{
//			if (PlayerPrefs.GetInt("deathcount", 0) % 3 == 0)
//			{
//				VideoInterstitial.SetActive(true);
//			}
//		}

		if (activeUI != null && !clearHistory)
		{
			screenHistory.Push(activeUI.name);
			Destroy(activeUI);
		}
		else if (clearHistory)
		{
			Destroy(activeUI);
		}

		activeUI = Instantiate((GameObject)Resources.Load(newUI));

		if (activeUI.name.EndsWith(cloneString))
		{
			activeUI.name = activeUI.name.Substring(0, activeUI.name.Length - cloneString.Length);
		}

		if (clearHistory)
		{
			screenHistory.Clear();
		}
	}

	public void GoBack()
	{
		if (screenHistory.Count == 0)
		{
			Application.Quit();
			return;
		}
		string previousScreen = screenHistory.Pop();

		if (activeUI != null)
		{
			Destroy(activeUI);
		}
		activeUI = Instantiate((GameObject)Resources.Load(previousScreen));
		if (activeUI.name.EndsWith(cloneString))
		{
			activeUI.name = activeUI.name.Substring(0, activeUI.name.Length - cloneString.Length);
		}
	
	}

	void printHistory()
	{
		string historyString = "history : ";
		foreach (var scene in screenHistory)
		{
			historyString = historyString + scene + ", ";
		}
		print(historyString);
	}

	#if UNITY_ANDROID || UNITY_EDITOR
		void Update()
		{
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				GoBack();
			}
		}
	#endif

	public void OnRatePressed()
	{
		PlayerPrefs.SetInt(Viker_prefsKeys.hasRated, 1);
	}
}
