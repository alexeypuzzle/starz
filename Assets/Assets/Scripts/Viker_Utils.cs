﻿using UnityEngine;
using System;

public class Viker_Utils : MonoBehaviour {

    public static bool isOnline(){
        return (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork || Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork);
    }

	public static Vector2 roundDownPos(Vector2 vector)
	{
		return new Vector2(Mathf.Floor(vector.x), Mathf.Floor(vector.y));
	}

    public static string getCleanPrefabName(string prefabString){
        if (prefabString.Contains(" ")){
            int indexOfSpace = prefabString.IndexOf(" ");
            return prefabString.Substring(0, indexOfSpace);
            // clean it.
        }
        return prefabString;
    }

    public static string getCleanCloneName(string fullName){
		if (fullName.Contains("("))
		{
			int indexOfBracket = fullName.IndexOf("(");
			return fullName.Substring(0, indexOfBracket);
		}
		return fullName;
    }
}


[Serializable]
public struct float4
{
	public float r;
	public float g;
	public float b;
	public float a;

	public float4(Color color)
	{
		r = color.r;
		g = color.g;
		b = color.b;
		a = color.a;
	}
}

[Serializable]
public struct float3
{
	public float x;
	public float y;
	public float z;

	public float3(Vector3 vector)
	{
		x = vector.x;
		y = vector.y;
		z = vector.z;
	}
}