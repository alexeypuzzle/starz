﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using TMPro;
using UnityEngine.Video;

public class Viker_mainMenu : MonoBehaviour
{
    public Button GetBallsButton;
    public TMP_Text SkipButtonText;
    public Text BallRewardText;
    public Text StarRewardText;
    public Image SkipButtonImage;
    public Image SkipBallImage;
    public Image SkipStarImage;
    public GameObject LoadingBalls;
    public GameObject SkipReady;
    public GameObject StopitPolaroid;
    public VideoClip video;
    public Text BestScoreText;
    int BestScore { get { return PlayerPrefs.GetInt(Viker_prefsKeys.HighScore); } }


    void Start()
    {
        StopitPolaroid.SetActive(Viker_UIController.instance.NumberOfGameLoads > 2);
        Pooler.instance.ResetPools();
        Time.timeScale = 1f;
        Viker_adsController.instance.hideBanner();
        Viker_UIController.instance.numberOfStartingBalls = 0;
        Viker_UIController.instance.numberOfStartingStars = 0;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            PlayerPrefs.SetInt(Viker_prefsKeys.HasRemovedAds, 1);
        }
    }

    void OnEnable()
    {
        UpdateBestScoreText();
//        StartCoroutine(DoSkipButtonChecks());
        if(PlayerPrefs.GetInt(Viker_prefsKeys.HighScore,0) < 10 || !Viker_Utils.isOnline()){
            GetBallsButton.gameObject.SetActive(false);
        } else{
            setShortcutValues();
            SkipReady.SetActive(true);
            LoadingBalls.SetActive(false);
            GetBallsButton.gameObject.SetActive(true);
        }
    }

	IEnumerator DoSkipButtonChecks()
	{
	    if (PurchaseController.HasRemovedAds())
	    {
	        SkipReady.SetActive(true);
	        LoadingBalls.SetActive(false);
	        yield break; // just show the button if ads have been removed.
	    }

	    while (!Viker_adsController.instance.IsVideoReady(Viker_adsController.SkipToLevel))
	    {
	        SkipReady.SetActive(false);
	        LoadingBalls.SetActive(true);
	        yield return new WaitForSeconds(0.5f);
	    }
	    SkipReady.SetActive(true);
	    LoadingBalls.SetActive(false);
	}

    public void OnAmazeballsBannerPressed(){
        Application.OpenURL(Viker_URLS.GamePromo);
	}

    void setShortcutValues(){
		SkipButtonText.text = "Skip to level " + getStarRewardCount();
		BallRewardText.text = "x" + getBallRewardCount();
		StarRewardText.text = "x" + getStarRewardCount();
    }

    public void OnGetBallsPressed(){
        SoundManager.instance.playBallpop();
		Viker_UIController.instance.numberOfStartingStars = getStarRewardCount();
		Viker_UIController.instance.numberOfStartingBalls = getBallRewardCount();
        if (!PurchaseController.HasRemovedAds())
        {
            print("hasn't removed ads. show.");
            Viker_adsController.OnVideoAdFinished += OnVideoWatched;
            Viker_adsController.instance.ShowRewardedVideo();
        } 
        else
        {
            Viker_UIController.instance.LoadUI(Viker_UI.Gameplay);    
        }   
	}

    void OnVideoWatched()
    {
        Viker_adsController.OnVideoAdFinished -= OnVideoWatched;
        Viker_UIController.instance.LoadUI(Viker_UI.Gameplay);
    }

    int getBallRewardCount(){
        return getStarRewardCount() * 3;
    }

    int getStarRewardCount(){
        return (int)Math.Ceiling((decimal)(PlayerPrefs.GetInt(Viker_prefsKeys.HighScore)/ 2) / 5) * 5;
    }

    public void onPlayPressed()
    {
        SoundManager.instance.playBallpop();
        Viker_UIController.instance.LoadUI(Viker_UI.Gameplay);
    }

    public void onNewsPressed(){
        SoundManager.instance.playBallpop();
        Viker_UIController.instance.ShowCrossPromo(true);
    }

    public void onSettingsPressed()
    {
        SoundManager.instance.playBallpop();
        Viker_UIController.instance.ShowHomepageSettings(true);
    }

    public void onNoAdsPressed()
    {
        SoundManager.instance.playBallpop();
        PurchaseController.instance.BuyNoAds();
    }

    public void onSharePressed()
    {
        SoundManager.instance.playBallpop();
    }

    public void onRatePressed()
    {
        SoundManager.instance.playBallpop();
#if UNITY_IOS
        Application.OpenURL(Viker_URLS.AppleStore);
#elif UNITY_ANDROID
        Application.OpenURL(Viker_URLS.GooglePlayStore);
#endif
	}
    
    void UpdateBestScoreText()
    {
        BestScoreText.text = BestScore.ToString();
    }
}
