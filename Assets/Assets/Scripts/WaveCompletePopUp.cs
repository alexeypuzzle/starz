﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Facebook.Unity;

public class WaveCompletePopUp : MonoBehaviour
{
    int waveNo = 0;
    int winAmount;
    int prize;
    public Text WaveCompleteText;
    public Text amountOfThings;
    public ParticleSystem Explosion;


    public Sprite ballIcon;
    public Image currentIcon;

    public GameObject StarForPopup;
    public GameObject BallForPopup;

    public GameObject popUp;

    private enum possiblePrizes { balls, stars };
    private enum colours { r, g, b };
    public Image Prizebutton;
    int colVal = (int)colours.g;
    bool colourChanged = false;

    void OnEnable()
    {
        Explosion.Play();
        waveNo++;
        WaveCompleteText.text = "LEVEL " + waveNo.ToString() + " COMPLETE!";
        determinePrizeAndAmount();
        TransitionColor();
        popUp.SetActive(true);
    }
    
    void SetListenersActive(bool areActive)
    {
        if (areActive)
        {
            Viker_adsController.OnInterstitialClosed += OnInterstitialFinished;
            
            Viker_adsController.OnShowInterstitialFailed += carryOnFailed;    
        }
        else
        {
            Viker_adsController.OnInterstitialClosed -= OnInterstitialFinished;
            Viker_adsController.OnShowInterstitialFailed -= carryOnFailed;
        } 
    }

    void determinePrizeAndAmount()
    {
        //increase balls
        prize = (int)possiblePrizes.balls;
        winAmount = UnityEngine.Random.Range(Mathf.RoundToInt(Viker_Gameplay.instance.BallCount * 0.2f), Mathf.RoundToInt(Viker_Gameplay.instance.BallCount * 0.3f));
        winAmount = Mathf.RoundToInt(winAmount / 5) * 5;
        if(winAmount < 5)
        {
            winAmount = 5;
        }
        amountOfThings.text = "+" + winAmount.ToString();
        currentIcon.sprite = ballIcon;
    }


    public void OnKeepGoingPressed()
    {
        if (!GameStateController.instance.HasRemovedAds)
        {
            SetListenersActive(true);
            Viker_adsController.instance.ShowInterstitial();
            popUp.SetActive(false);
        }
        else
        {
            OnInterstitialFinished();
//            Viker_Gameplay.instance.OnGoodJobKeepPlayingFinished();
            popUp.SetActive(false);
            //          gameObject.SetActive(false);
        }
    }

    void TransitionColor()
    {
        iTween.ValueTo(gameObject, iTween.Hash("from", 0f, "to", 1f, "time", 2f, "OnUpdateTarget", gameObject, "OnUpdate", "colourUpdate", "looptype", "pingpong"));

    }

    void colourUpdate(float  _val)
    {
        Prizebutton.alterR(_val);
    }

    void OnInterstitialFinished()
    {
        if(prize == (int)possiblePrizes.balls)
        {
            //TODO add a function to add ballz to the total ball count in game for rewarded thangs...
            //do a thing... sort this plz...
            Viker_Gameplay.instance.addballs(winAmount);
        }
        else if (prize == (int)possiblePrizes.stars)
        {
            Viker_Gameplay.instance.AddStarsToScore(winAmount);
        }

		LogCollectedMidgameRewardEvent();

		SetListenersActive(false);

        carryOn();
        //TODO add facebook event here
    }

	public void LogCollectedMidgameRewardEvent()
    {
        FB.LogAppEvent(
            "Collected Midgame Reward"
        );
    }

    IEnumerator SpawnReward()
    {
        GameObject prizetype;
        if(prize == (int)possiblePrizes.balls)
        {
            prizetype = BallForPopup;
        }
        else
        {
            prizetype = StarForPopup;
        }
        for (int i = 0; i < winAmount; i++)
        {
            Instantiate(prizetype, transform.position, Quaternion.identity);
            yield return new WaitForSeconds(0.1f);
        }
        Viker_Gameplay.instance.OnGoodJobKeepPlayingFinished();
    }

    void carryOn()
    {
        StartCoroutine(SpawnReward());
    }

    public void carryOnFailed()
    {
        Viker_Gameplay.instance.OnGoodJobKeepPlayingFinished();
        SetListenersActive(false);
    }
    

}