﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public static class ExtensionMethods
{

    public static void Rotate2D(this Transform trans, float _z)
    {
        Quaternion crappyDesign = trans.rotation;
        crappyDesign.eulerAngles = new Vector3(trans.rotation.eulerAngles.x, trans.rotation.eulerAngles.y, _z);
        trans.rotation = crappyDesign;
        //		trans.rotation.eulerAngles
    }
    #region Regular Transform
    public static void moveX(this Transform trans, float _x)
    {
        trans.position = new Vector3(_x, trans.position.y, trans.position.z);
    }

    public static void moveY(this Transform trans, float _y)
    {
        trans.position = new Vector3(trans.position.x, _y, trans.position.z);
    }

    public static void moveZ(this Transform trans, float _z)
    {
        trans.position = new Vector3(trans.position.x, trans.position.y, _z);
    }
    #endregion

    #region Rect Transform
    public static void moveX(this RectTransform trans, float _x)
    {
        trans.anchoredPosition = new Vector2(_x, trans.anchoredPosition.y);
    }

    public static void moveY(this RectTransform trans, float _y)
    {
        trans.anchoredPosition = new Vector2(trans.anchoredPosition.x, _y);
    }

    public static void LookAt2D(this Transform trans, GameObject _target)
    {
        Vector3 diff = _target.transform.position - trans.position;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        trans.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
    }
    #endregion

    #region UI colours
    public static void alterR(this Image img, float _r)
    {
        img.color = new Color(_r, img.color.g, img.color.b);
    }

    public static void alterG(this Image img, float _g)
    {
        img.color = new Color(img.color.r, _g, img.color.b);
    }

    public static void alterB(this Image img, float _b)
    {
        img.color = new Color(img.color.r, img.color.g, _b);
    }

    public static void alterA(this Image img, float _a)
    {
        img.color = new Color(img.color.r, img.color.g, img.color.b, _a);
    }

    public static void alterR(this Text text, float _r)
    {
        text.color = new Color(_r, text.color.g, text.color.b);
    }

    public static void alterG(this Text text, float _g)
    {
        text.color = new Color(text.color.r, _g, text.color.b);
    }

    public static void alterB(this Text text, float _b)
    {
        text.color = new Color(text.color.r, text.color.g, _b);
    }

    public static void alterA(this Text text, float newAlpha)
    {
        text.color = new Color(text.color.r, text.color.g, text.color.b, newAlpha);
    }
    #endregion

    //#region PlayerPrefs
    //public static void incrementInt(this PlayerPrefs plaPre, string Key, int incrementValue)
    //{
        
    //}

    //#endregion

}
