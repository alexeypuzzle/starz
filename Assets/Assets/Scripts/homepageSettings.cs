﻿using UnityEngine;
using UnityEngine.UI;

public class homepageSettings : MonoBehaviour {

	public Toggle soundToggle;

	private void Start()
	{
		soundToggle.isOn = (PlayerPrefs.GetInt(Viker_prefsKeys.Sound, 1) == 1);
	}

	public void OnSoundToggled(bool isOn)
	{
        SoundManager.instance.playBallpop();
		PlayerPrefs.SetInt(Viker_prefsKeys.Sound, (isOn) ? 1 : 0);
		soundToggle.isOn = isOn;
	}

    public void OnRestorePurchasesPressed(){
        SoundManager.instance.playBallpop();
        PurchaseController.instance.RestorePurchases();
    }

    public void OnClosePressed(){
        SoundManager.instance.playBallpop();
        Viker_UIController.instance.ShowHomepageSettings(false);
    }

}
