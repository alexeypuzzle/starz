﻿using UnityEngine;
using System.Collections;

public class spin : MonoBehaviour {

    public float anglePerFrame;
    Transform child;

    private void OnEnable()
    {
        child = transform.GetChild(0);
        StartCoroutine("spinMe");
    }

    IEnumerator spinMe(){
        
        while (true)
        {
			child.Rotate(new Vector3(0f, 0f, anglePerFrame));
			yield return new WaitForSeconds(0.01f);     
        }
    }
    
}
