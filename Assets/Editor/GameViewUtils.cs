﻿using System.Collections.Generic;
using System.Reflection;
using UnityEditor;

public class GameViewUtils
     {
         static object gameViewSizesInstance;
         static MethodInfo getGroup;
         static string iPadResolution = "768x1024";
         static string iPhone7Resolution = "750x1334";  
         static string iPhoneXResolution = "1125x2436";  

         static GameViewUtils()
         {   
             //use reflection to get a reference to the instance of the game view sizes singleton
             var sizesType = typeof(Editor).Assembly.GetType("UnityEditor.GameViewSizes");
             var singleType = typeof(ScriptableSingleton<>).MakeGenericType(sizesType);
             var instanceProp = singleType.GetProperty("instance");
             getGroup = sizesType.GetMethod("GetGroup");
             gameViewSizesInstance = instanceProp.GetValue(null, null); // magic where we grab the singleton instance reference. 
         }
     
         private static void SetSize(int index)
         {
             //get the game view
             var gvWndType = typeof(Editor).Assembly.GetType("UnityEditor.GameView");
             var gvWnd = EditorWindow.GetWindow(gvWndType);
             
             
             var SizeSelectionCallback = gvWndType.GetMethod("SizeSelectionCallback",
                 BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
             SizeSelectionCallback.Invoke(gvWnd, new object[] { index, null });
         }
     
         static object GetGroup(GameViewSizeGroupType type)
         {
             return getGroup.Invoke(gameViewSizesInstance, new object[] { (int)type });
         }

         [MenuItem("Tools/GameViewSize/iPhoneX  &%3")]
         static void SetIphoneXRes()
         {
             SetSize(GetMenuIndexForResolution(iPhoneXResolution));
         }
         
         
         [MenuItem("Tools/GameViewSize/iPhone7  &%2")]
         static void SetIPhone7Res()
         {
             SetSize(GetMenuIndexForResolution(iPhone7Resolution));
         }
         
         
         [MenuItem("Tools/GameViewSize/iPad  &%1")]
         static void SetIpadResolution()
         {
             SetSize(GetMenuIndexForResolution(iPadResolution));
         }
         

         static int GetMenuIndexForResolution(string resolutionString)
         {
             int index = 0;
             List<string> availableResolutions = GetAvailableResolutions();
             foreach (var res in availableResolutions)
             {
                 if (res.Contains(resolutionString))
                 {
                     return availableResolutions.IndexOf(res);
                 }
             }
             return index;
         }
         
         static List<string> GetAvailableResolutions()
         {
             List<string> availableSizes = new List<string>();
             // only support android and ios for now!
             GameViewSizeGroupType currentGameView = EditorUserBuildSettings.activeBuildTarget == BuildTarget.Android
                 ? GameViewSizeGroupType.Android
                 : GameViewSizeGroupType.iOS;
             var group = GetGroup(currentGameView);
             var getDisplayTexts = group.GetType().GetMethod("GetDisplayTexts");
             string[] sizes = getDisplayTexts.Invoke(group, null) as string[];
             if (sizes != null)
                 foreach (var size in sizes)
                 {
                     availableSizes.Add(size);
                 }
             return availableSizes;
         }
         
         
     }
     
     