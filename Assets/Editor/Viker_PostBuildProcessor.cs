﻿
using UnityEngine;
using UnityEditor.Callbacks;
using UnityEditor;
using System.IO;
using UnityEditor.iOS.Xcode;

public class Viker_PostBuildProcessor:MonoBehaviour {

//#if UNITY_CLOUD_BUILD && UNITY_IOS
// This method is added in the Advanced Features Settings on UCB
// Viker_PostBuildProcessor.OnPostprocessBuildiOS

    public static void OnPostprocessBuildiOS (string exportPath)
    {
//    Debug.Log(" Alexey - [UNITY_CLOUD_BUILD] OnPostprocessBuildiOS");
//        ADCPostBuildProcessor.UpdateProject(BuildTarget.iOS, exportPath);
//        VunglePostBuilder.onPostProcessBuildPlayer(BuildTarget.iOS, exportPath);
//        FyberPostProcessBuild.OnPostProcessBuild(BuildTarget.iOS, exportPath);
//        MoPubPostBuildiOS.OnPostprocessBuild(BuildTarget.iOS, exportPath);
//        ProcessPostBuild(BuildTarget.iOS,exportPath);
    }
    
    [PostProcessBuild]
    public static void OnPostprocessBuild (BuildTarget buildTarget, string path)
    {
#if !UNITY_CLOUD_BUILD
        Debug.Log ("[LOCAL BUILD] OnPostprocessBuild");
        ProcessPostBuild (buildTarget, path);
#endif
    }
    
    static void ProcessPostBuild(BuildTarget buildTarget, string path)
    {
        // This code will set modules to enabled in Xcode build settings
#if UNITY_IOS

//        Debug.Log ("[UNITY_IOS] ProcessPostBuild - Xcode Manipulation API");
//
//        // Go get pbxproj file
//        string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
//
//        // PBXProject class represents a project build settings file,
//        // here is how to read that in.
//        PBXProject proj = new PBXProject ();
//        proj.ReadFromFile (projPath);
//
//        // This is the Xcode target in the generated project
//        string target = proj.TargetGuidByName ("Unity-iPhone");
//
//        // Here we go: Set 'Enable Modules' to YES to prevent errors using @import syntax
//        Debug.Log("Enabling modules: CLANG_ENABLE_MODULES = YES");
//        proj.AddBuildProperty(target, "CLANG_ENABLE_MODULES", "YES");
//
//        // Write PBXProject object back to the file
//        proj.WriteToFile (projPath);
//string tempfile = "gradle.properties";
#endif
    }
    
    
//#endif
    
// 
//    static void ProcessPostBuild(BuildTarget buildTarget, string path)
//    {
//        // This code will set modules to enabled in Xcode build settings
//#if UNITY_IOS
//        Debug.Log("alexey - 2");
//        Debug.Log ("[UNITY_IOS] ProcessPostBuild - Xcode Manipulation API");
//
//        // Go get pbxproj file
//        string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
//
//        // PBXProject class represents a project build settings file,
//        // here is how to read that in.
//        PBXProject proj = new PBXProject ();
//        proj.ReadFromFile (projPath);
//        Debug.Log("alexey - 2.5");
//        // This is the Xcode target in the generated project
//        string target = proj.TargetGuidByName ("Unity-iPhone");
//        Debug.Log("alexey - 3");
//        // Here we go: Set 'Enable Modules' to YES to prevent errors using @import syntax
//        Debug.Log("Enabling modules: CLANG_ENABLE_MODULES = YES");
//        proj.AddBuildProperty(target, "CLANG_ENABLE_MODULES", "YES");
//        Debug.Log("alexey - 4");
//        // Write PBXProject object back to the file
//        proj.WriteToFile (projPath);
//        Debug.Log("alexey - 5");
//
//#endif
//    }
    
    
}