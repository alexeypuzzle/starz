﻿using com.adjust.sdk;
using UnityEngine;

public class AdjustInitializer : MonoBehaviour
{

	public string AndroidToken;
	public string IOSToken;

	public AdjustEnvironment Environment;

	void Awake()
	{
		string token;
#if UNITY_IOS
token = IOSToken;
		#elif UNITY_ANDROID
		token = AndroidToken;
#endif
		AdjustConfig config = new AdjustConfig(token, Environment);
		Adjust.start(config);
	}
}
