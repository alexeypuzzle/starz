﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideOnIpad : MonoBehaviour {

	// Use this for initialization
	#if UNITY_IOS
	void Start () {
		if((UnityEngine.iOS.Device.generation.ToString()).IndexOf("iPad") > -1){
			gameObject.SetActive(false);
		}	
	}
	#endif
}
