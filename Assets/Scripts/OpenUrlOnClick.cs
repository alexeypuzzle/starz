﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class OpenUrlOnClick : MonoBehaviour
{

	public string iosUrl;
	public string androidUrl;

	string url
	{
		get
		{
			#if UNITY_IOS
			return iosUrl;
#elif UNITY_ANDROID
			return androidUrl;
#endif
		}
	}

	void Start()
	{
		GetComponent<Button>().onClick.AddListener(delegate { Application.OpenURL(url); });
	}
}
