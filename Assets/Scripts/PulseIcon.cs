﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulseIcon : MonoBehaviour {

	// Use this for initialization
	void Start () {
        iTween.ScaleBy(gameObject, iTween.Hash("amount", new Vector3(1.3f, 1.3f, 0f), "time", 0.5f, /*"looptype", iTween.LoopType.loop,*/ "easetype", iTween.EaseType.easeOutElastic, "looptype", iTween.LoopType.pingPong));
	}
}
