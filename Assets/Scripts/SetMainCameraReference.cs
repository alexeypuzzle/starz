﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMainCameraReference : MonoBehaviour {
	void Awake()
	{
		GetComponent<Canvas>().worldCamera = Viker_UIController.instance.MainCam;
	}
}
