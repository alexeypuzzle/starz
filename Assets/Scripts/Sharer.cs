﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sharer : MonoBehaviour
{

	[SerializeField] string _shareText = "Check out Ballz vs Starz! http://bit.ly/BallzvsStarziOS";
	
	public void MainMenuShare()
	{
		#if !UNITY_EDITOR
NativeShare.Share(_shareText);
#else
		Debug.LogWarning("Can't share from Editor.");
		#endif
		
	}
}
