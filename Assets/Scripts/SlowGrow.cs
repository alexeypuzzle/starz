﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowGrow : MonoBehaviour {

    [SerializeField] float GrowTimeOneWay;
	// Use this for initialization
	void Start () {
        iTween.ScaleTo(gameObject, iTween.Hash("x", 1.1f, "y", 1.1f, "time", GrowTimeOneWay, "looptype", iTween.LoopType.pingPong, "easetype", iTween.EaseType.linear));
	}
}
