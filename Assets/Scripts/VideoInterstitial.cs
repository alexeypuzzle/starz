﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

[RequireComponent(typeof(VideoPlayer))]
public class VideoInterstitial : MonoBehaviour
{
	public Image ImageToShowAfterVideo;
	VideoPlayer _player;
	float _videoLength;

	void Awake()
	{
		_player = GetComponent<VideoPlayer>();
	}

	void OnEnable()
	{
		ImageToShowAfterVideo.gameObject.SetActive(false);
		_videoLength = (float)_player.clip.length;
		StartCoroutine(ShowImage());
		_player.Play();
	}

	IEnumerator ShowImage()
	{
		yield return new WaitForSeconds(_videoLength);
		ImageToShowAfterVideo.gameObject.SetActive(true);
	}

	void OnDisable()
	{
		_player.Stop();
	}
}
