﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

[RequireComponent(typeof(VideoPlayer))]
public class VideoPlayerManager : MonoBehaviour
{
	[Tooltip("FileName In Streaming Assets Folder")]
	public string videoName;
	public Image ImageToShowAfterVideo;
	public float showStillAfterSeconds;
	VideoPlayer _player;
	float _videoLength;

	void Awake()
	{
		_player = GetComponent<VideoPlayer>();
	}
	
	void OnEnable()
	{
		ImageToShowAfterVideo.gameObject.SetActive(false);
		StartCoroutine(FetchVideo());
		StartCoroutine(ShowImage());
		
	}

	IEnumerator ShowImage()
	{
		yield return new WaitForSeconds(showStillAfterSeconds);
		ImageToShowAfterVideo.gameObject.SetActive(true);
	}
	
	IEnumerator FetchVideo()
	{
		_player.source = VideoSource.Url;
		#if UNITY_IOS && !UNITY_EDITOR
				string path = Application.dataPath + "/Raw/" + videoName + ".mp4";
		#elif UNITY_ANDROID && !UNITY_EDITOR
				string path = "jar:file://" + Application.dataPath + "!/assets/" + videoName + ".mp4";
		#elif UNITY_EDITOR
		string path = Application.streamingAssetsPath + "/" + videoName + ".mp4";
		#endif
		_player.url = path;
		yield break;
	}

	void OnDisable()
	{
		_player.Stop();
	}
	
}
