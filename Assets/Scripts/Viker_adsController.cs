﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using GoogleMobileAds.Api;
using UnityEngine.Advertisements;

public class Viker_adsController : MonoBehaviour
{

    public static string ContinuePlaying = "continuePlaying";
    public static string SkipToLevel = "skipToLevel";

    #region Events
    public delegate void Eventhandler();

    /// <summary>
    /// Video watched to the end
    /// </summary>
    public static event Eventhandler OnVideoAdFinished;

    /// <summary>
    /// Video interrupted (closed by user, phone call, app close etc...)
    /// </summary>
    public static event Eventhandler OnVideoAdAborted;

    /// <summary>
    /// Failed to show a video
    /// </summary>
    public static event Eventhandler OnShowVideoAdFailed;

    /// <summary>
    /// Interstitial closed. Can be closed during or after show.
    /// </summary>
    public static event Eventhandler OnInterstitialClosed;

    /// <summary>
    /// tried to load an interstitial without a connection or received an error.
    /// </summary>
    public static event Eventhandler OnShowInterstitialFailed;
    #endregion

    #region Variables
    public static Viker_adsController instance;
    const string AdLoadingScreenPrefabString = "AdLoadingScreen";
    GameObject AdLoadingScreen;

    [Header("Unity Ads")]
    [SerializeField] string AppleStoreID;
    [SerializeField] string PlayStoreID;

    [SerializeField] bool TestModeActive;
    const string _testiOSBanner = "ca-app-pub-3940256099942544/2934735716";
    const string _testiOSInter = "ca-app-pub-3940256099942544/4411468910";
    const string _testAndroidBanner = "ca-app-pub-3940256099942544/6300978111";
    const string _testAndroidInter = "ca-app-pub-3940256099942544/1033173712";

    [Header("iOS")]
    [SerializeField] string _iosAppId;
    [SerializeField] string _iosBanner;

    [Header("Android")]
    [SerializeField] string _androidAppId;
    [SerializeField] string _androidBanner;

    [Header("General Settings")]
    [SerializeField] float _loadingBufferInSeconds = 10f;
    [SerializeField] float _adTimeoutLimit = 5f;
    [SerializeField] string _loadingText = "Loading...";
    [SerializeField] string _loadingFailedText = "Ad failed to load. Please try again.";
    [SerializeField] bool ShowAdSuccessInUnityEditor;

    public bool IsInterstitialReady { get { return _interstitial != null && _interstitial.IsLoaded(); } }

    string AppId
    {
        get
        {
#if UNITY_IOS
            return _iosAppId;
#elif UNITY_ANDROID
            return _androidAppId;
#endif
        }
    }

    string BannerId
    {
        get
        {
#if UNITY_IOS
            return TestModeActive ? _testiOSBanner : _iosBanner;
#elif UNITY_ANDROID
            return TestModeActive ? _testAndroidBanner : _androidBanner;
#endif
        }
    }

    string UnityGameID
    {
        get
        {
#if UNITY_IOS
            return AppleStoreID;
#elif UNITY_ANDROID
            return PlayStoreID;
#endif
        }
    }

    BannerView _bannerView;
    InterstitialAd _interstitial;

    float _timeAtGameLoad;
    bool _isBannerLoaded;
    Text _loadingScreenText;
    IEnumerator _currentWaitingTimer;
    string _interstitialId;
    string _videoID;

    // Banner states
    bool _isWaitingForBannerResponse;
    bool _isBannerShowing;
    bool _hidingBannerForAd;

    // Interstitial states
    bool _isWaitingForInterstitialResponse;
    bool _isInterstitialReady;
    bool _isInterstitialStarted;

    #endregion

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        transform.SetParent(transform.root.parent);
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        Advertisement.Initialize(UnityGameID, TestModeActive);
        MobileAds.Initialize(AppId);
        this.requestBanner();
        CreateAdLoadingScreen();
    }

    #region Interstitial

    public void ShowInterstitial()
    {
        if (Advertisement.IsReady("skippableAd"))
        {
            ShowOptions options = new ShowOptions();
            options.resultCallback = HandleShowResultSkippable;
            Advertisement.Show("skippableAd", options);
        }
        else
        {
            Debug.Log("video not ready");
        }
    }

    #endregion

    #region Video

    public void ShowRewardedVideo()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            ShowOptions options = new ShowOptions();
            options.resultCallback = HandleShowResultRewarded;
            Advertisement.Show("rewardedVideo", options);
        }
        else
        {
            Debug.Log("video not ready");
        }
    }

    public bool IsVideoReady(string videoId)
    {
        return Advertisement.IsReady(videoId);
    }

    void HandleShowResultRewarded(ShowResult result)
    {
        if (result == ShowResult.Skipped)
        {

            Debug.LogWarning("Video was skipped - Do NOT reward the player");
            if (OnVideoAdAborted != null) OnVideoAdAborted();

        }
        else if (result == ShowResult.Finished)
        {
            Debug.Log("Video completed - Offer a reward to the player");
            if (OnVideoAdFinished != null) OnVideoAdFinished();
            // Reward your player here.

        }
        else if (result == ShowResult.Failed)
        {
            Debug.LogError("Video failed to show");
            if (OnShowVideoAdFailed != null) OnShowVideoAdFailed();
        }
    }

    void HandleShowResultSkippable(ShowResult result)
    {
        if (result == ShowResult.Skipped)
        {

            Debug.LogWarning("Video was skipped - Do NOT reward the player");
            if (OnInterstitialClosed != null) OnInterstitialClosed();

        }
        else if (result == ShowResult.Failed)
        {
            Debug.LogError("Video failed to show");
            if (OnShowInterstitialFailed != null) OnShowInterstitialFailed();
        }
    }

    #endregion

    #region Banner

    void requestBanner()
    {
        Debug.Log("~Setting up banner view");
        _bannerView = new BannerView(BannerId, AdSize.Banner, AdPosition.Bottom);
        Debug.Log("~Banner view set");


        Debug.Log("~Setting events");
        _bannerView.OnAdLoaded += HandleOnAdLoaded;
        _bannerView.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        _bannerView.OnAdOpening += HandleOnAdOpened;
        _bannerView.OnAdClosed += HandleOnAdClosed;
        _bannerView.OnAdLeavingApplication += HandleOnAdLeavingApplication;
        Debug.Log("~Events Set");

        Debug.Log("~Requesting Ad");
        AdRequest request = new AdRequest.Builder().Build();
        _bannerView.LoadAd(request);
        Debug.Log("~Ad requested");
    }

    public void showBanner()
    {
        Debug.Log("~Showing Ad");
        _bannerView.Show();

    }

    public void hideBanner()
    {
        _bannerView.Hide();
    }

    public void destroyBanner()
    {
        _bannerView.Destroy();
    }

    public void HandleOnAdLoaded(object _sender, System.EventArgs _args)
    {
        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleOnAdFailedToLoad(object _sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
                            + args.Message);
    }

    public void HandleOnAdOpened(object _sender, System.EventArgs _args)
    {
        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleOnAdClosed(object _sender, System.EventArgs _args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleOnAdLeavingApplication(object _sender, System.EventArgs _args)
    {
        MonoBehaviour.print("HandleAdLeavingApplication event received");
    }

    #endregion

    void OnInterClosed(object sender, EventArgs args)
    {
        print("inter closed");
        if (OnInterstitialClosed != null) OnInterstitialClosed();
    }

    void OnInterLoadSuccess(object sender, EventArgs args)
    {
        _isInterstitialReady = true;
        _isWaitingForInterstitialResponse = false;
        print("interstitial load success.");
    }

    void OnInterLoadFail(object sender, AdFailedToLoadEventArgs args)
    {
        _isInterstitialReady = false;
        _isWaitingForInterstitialResponse = false;
        if (OnShowInterstitialFailed != null) OnShowInterstitialFailed();
        print("interstitial load fail. Message: " + args.Message);
    }

    #region Loading Screen


    void CreateAdLoadingScreen()
    {
        AdLoadingScreen = Instantiate(Resources.Load(AdLoadingScreenPrefabString), Camera.main.transform.parent, false) as GameObject;
        AdLoadingScreen.transform.SetAsLastSibling();
        _loadingScreenText = AdLoadingScreen.GetComponentInChildren<Text>();
    }

    void SetLoadingScreenActive(bool isActive)
    {
        if (isActive)
        {
            SetLoadingText();
        }
        AdLoadingScreen.SetActive(isActive);
    }

    void SetLoadingText()
    {
        _loadingScreenText.text = _loadingText;
    }

    void SetLoadingFailedText()
    {
        _loadingScreenText.text = _loadingFailedText;
    }

    IEnumerator HideLoadingScreenWithDelay()
    {
        yield return new WaitForSeconds(2f);
        SetLoadingScreenActive(false);
    }



    #endregion

    bool isOnline()
    {
        return Application.internetReachability != NetworkReachability.NotReachable;
    }

    public bool IsLoadingBufferComplete()
    {
        return Time.time - _timeAtGameLoad > _loadingBufferInSeconds;
    }

    IEnumerator WaitForBufferComplete(Action OnComplete)
    {
        while (!IsLoadingBufferComplete())
        {
            yield return new WaitForSeconds(0.2f);
        }
        OnComplete();
    }
}