﻿using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class iphoneXAdjust : MonoBehaviour
{

    public Vector2 iPhoneXPos;
    RectTransform rect;

    void Awake()
    {
        rect = GetComponent<RectTransform>();
    }

    void Start()
    {
        if (IsIphoneX())
        {

            rect.anchoredPosition = iPhoneXPos;
        }
    }

    public static bool IsIphoneX()
    {
        return Screen.height == 2436 && Screen.width == 1125;
    }
}
