﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class iphoneXCameraAdjust : MonoBehaviour
{

    public Vector3 iphonexCameraPosition;
    public float iphoneXcameraSize;
    Camera cam;
    

    void Awake()
    {
        cam = GetComponent<Camera>();
    }

    void Start()
    {
        if (iphoneXAdjust.IsIphoneX())
        {
            transform.position = iphonexCameraPosition;
            cam.orthographicSize = iphoneXcameraSize;
        }
    }
}
