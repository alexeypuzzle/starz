﻿using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class iphoneXchangeSize : MonoBehaviour
{
    RectTransform rect;
    public Vector2 iphoneXSize;

    void Awake()
    {
        rect = GetComponent<RectTransform>();
    }

    void Start()
    {
        if (iphoneXAdjust.IsIphoneX())
        {
            rect.sizeDelta = iphoneXSize;    
        }
    }
    
}
