﻿using UnityEngine;
using UnityEngine.EventSystems;

public class buttonTransition : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler 
{
	float originalScale;
	public float scaleto;


	void Start()
	{
		originalScale = gameObject.GetComponent<RectTransform> ().localScale.x;
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		iTween.ScaleTo (gameObject, iTween.Hash ("x", scaleto, "y", scaleto));
	}

	public void OnPointerExit(PointerEventData eventData)
	{

		iTween.ScaleTo(gameObject, iTween.Hash ("x", originalScale, "y", originalScale, "easeType", "easeOutElastic"));
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		iTween.ScaleTo (gameObject, iTween.Hash ("x", originalScale, "y", originalScale));
	}
}
