﻿using UnityEngine;

public class caroselController : MonoBehaviour
{

	public static caroselController Instance;
	Vector2 startPosition = Vector2.zero; 
	bool touchRegistered = false;
	public delegate void close();
	public static event close OnClosed;
	public GameObject NewsContainer;
	RectTransform _rectTransform;		

	Vector3 LeftPos = new Vector3(-20, 0, 0);
	Vector3 RightPos = new Vector3(20, 0, 0);

	int previousAd;
	bool limiter = false;
	int  currentAd, maxAds, ActiveAdSpaces; 

	void Awake()
	{
		Instance = this;
		_rectTransform = NewsContainer.GetComponent<RectTransform>();
		
		currentAd = 1;
	}

	void OnEnable()
	{
		_rectTransform.anchoredPosition = new Vector2(0f, Screen.height);
		iTween.ValueTo(gameObject, iTween.Hash("from", _rectTransform.anchoredPosition.y, "to", 0f, "onupdate", "UpdateRectY", "time", 0.5, "easetype", "easeOutBack"));
		currentAd = 1;
	}

	public void Show()
	{
		gameObject.SetActive(true);
	}

	public void UpdateRectY(float newY)
	{
		_rectTransform.anchoredPosition = new Vector2(0f, newY);
	}

	void Update()
	{
		#if UNITY_EDITOR
		if (Input.GetKey(KeyCode.RightArrow))
		{
			OnRight();
		} else if (Input.GetKey(KeyCode.LeftArrow))
		{
			OnLeft();
		}
		#endif
		
	
		
#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS) 
		Touch touchIn;
		if(Input.touchCount > 0)
		{
			touchIn = Input.GetTouch(0);


			if(touchIn.phase == TouchPhase.Began)
			{
				//touchIn.rawPosition = startPosition;
				startPosition = touchIn.position;
				//Vector2 realWorldPos = Camera.main.ScreenToWorldPoint(startPosition);
				//Debug.Log(startPosition);
			}

			if(touchIn.phase == TouchPhase.Moved && touchRegistered == false)
			{
				if(touchIn.position.x >= startPosition.x + 55)
				{
					//endPosition = touchIn.position;
					touchRegistered = true;
					OnLeft();
				}
				else if(touchIn.position.x <= startPosition.x - 55)
				{
					touchRegistered = true;
					OnRight();
				}
			}

			if(touchIn.phase == TouchPhase.Ended)
			{
				touchRegistered = false;
			}
		}

		# endif


	}


	public void OnLeft() 
	{
		if (!limiter) 
		{
			limiter = true;
			currentAdDecrement ();

//			InLeft (determineAd (currentAd), determineAd (previousAd)); 
		}
	}

	public void OnRight()
	{
		if (!limiter) 
		{
			limiter = true;
			currentAdIncrement ();

//			InRight (determineAd (currentAd), determineAd (previousAd)); 
		}
	}

	void currentAdDecrement()
	{
//		bool adworks = false;
//		previousAd = currentAd;
//		while (!adworks)
//		{
//			currentAd--;
//
//			if (currentAd < 1) 
//			{
//				currentAd = ActiveAdSpaces;
//			}
//			#if UNITY_ANDROID
//			if(determineAd(currentAd).GetComponent<menuAd>().PlaystoreURL != "")
//			{
//				adworks = true;
//			}
//			#endif
//			#if UNITY_IOS
////			if(determineAd(currentAd).GetComponent<menuAd>().AppstoreURL != "")
////			{
////				adworks = true;
////			}
//			#endif
//			Debug.Log ("returning" + currentAd); 
//		}
//		indicatorDown ();

	}

	void currentAdIncrement()
	{
//		bool adworks = false;
//		previousAd = currentAd;
//		while (!adworks)
//		{
//			currentAd++;
//
//			if (currentAd > ActiveAdSpaces) 
//			{
//				currentAd = 1;
//			}
//			#if UNITY_ANDROID
//			if(determineAd(currentAd).GetComponent<menuAd>().PlaystoreURL != "")
//			{
//				adworks = true;
//				Debug.Log("ad found android");
//			}
//			#endif
////			#if UNITY_IOS
////			if(determineAd(currentAd).GetComponent<menuAd>().AppstoreURL != "")
////			{
////				adworks = true;
////				Debug.Log("ad found ios");
////			}
////			#endif
//			Debug.Log ("returning" + currentAd); 
//		}
//		indicatorUp ();
	}

	void indicatorUp()
	{
//		if (CI1.isOn) 
//		{
//			if (CI2.gameObject.activeSelf) 
//			{
//				CI1.isOn = false;
//				CI2.isOn = true;
//			}
//		} 
//
//		else if (CI2.isOn) 
//		{
//			if (CI3.gameObject.activeSelf) 
//			{
//				CI2.isOn = false;
//				CI3.isOn = true;
//			} 
//			else 
//			{
//				CI1.isOn = true;
//				CI2.isOn = false;
//			}
//		} 
//
//		else if (CI3.isOn) 
//		{
//			if (CI4.gameObject.activeSelf) 
//			{
//				CI3.isOn = false;
//				CI4.isOn = true;
//			} 
//			else 
//			{
//				CI1.isOn = true;
//				CI3.isOn = false;
//			}
//		}
//
//		else if (CI4.isOn) 
//		{
//			CI1.isOn = true;
//			CI4.isOn = false;
//
//		}
	}

	void indicatorDown()
	{
//		if (CI1.isOn) 
//		{
//			if (CI4.gameObject.activeSelf) 
//			{
//				CI1.isOn = false;
//				CI4.isOn = true;
//			}
//			else if (CI3.gameObject.activeSelf) 
//			{
//				CI1.isOn = false;
//				CI3.isOn = true;
//			}
//			else if (CI2.gameObject.activeSelf) 
//			{
//				CI1.isOn = false;
//				CI2.isOn = true;
//			}
//		} 
//
//		else if (CI2.isOn) 
//		{
//			CI1.isOn = true;
//			CI2.isOn = false;
//		} 
//
//		else if (CI3.isOn) 
//		{
//			CI2.isOn = true;
//			CI3.isOn = false;
//		}
//
//		else if (CI4.isOn) 
//		{
//			CI3.isOn = true;
//			CI4.isOn = true;
//
//		}
	}

//	GameObject determineAd(int _ad)
//	{
//		switch (_ad) {
//		case 1:
//			{
//				return Ad1;
//			}
//		case 2:
//			{
//				return Ad2;
//			}
//		case 3:
//			{
//				return Ad3;
//			}
//		case 4:
//			{
//				return Ad4;
//			}
//		default:
//			{
//				Debug.Log ("OUT OF BOUNDS ERROR: RETURNING AD 1");
//				return Ad1;
//			}
//		}
//	}

	void InLeft(GameObject _newad, GameObject _oldad)
	{
			_newad.transform.position = LeftPos;
			_newad.SetActive (true);
			iTween.MoveTo(_newad, iTween.Hash("x", 0, "y", 0, "time", 0.5f));
			//iTween.ValueTo (_newad, iTween.Hash ("from", 0, "to", 1, "time", 0.5f, "onupdatetarget", gameObject, "onupdateparams", _newad, "onupdate", "alpha")); 
			iTween.MoveTo (_oldad, iTween.Hash ("x", 20, "y", 0, "time", 0.5f, "oncompletetarget", gameObject, "oncompleteparams", _oldad, "oncomplete", "turnoff"));
			//iTween.ValueTo (_newad, iTween.Hash ("from", 1, "to", 0, "time", 0.5f, "onupdatetarget", gameObject, "onupdateparams", _oldad, "onupdate", "alpha", "oncompletetarget", gameObject, "oncompleteparams", _oldad, "oncomplete", "turnoff")); 
	}

	void InRight(GameObject _newad, GameObject _oldad)
	{
			_newad.transform.position = RightPos;
			_newad.SetActive (true);
			iTween.MoveTo(_newad, iTween.Hash("x", 0, "y", 0, "time", 0.5f));
			//iTween.ValueTo (_newad, iTween.Hash ("from", 0, "to", 1, "time", 0.5f, "onupdatetarget", gameObject, "onupdate", "alphaUp", "easetype", "easeinexpo")); 
			iTween.MoveTo (_oldad, iTween.Hash ("x", -20, "y", 0, "time", 0.5f, "oncompletetarget", gameObject, "oncompleteparams", _oldad, "oncomplete", "turnoff"));
			//iTween.ValueTo (_newad, iTween.Hash ("from", 1, "to", 0, "time", 0.5f, "onupdatetarget", gameObject, "onupdate", "alphaDown", "easetype", "easeoutexpo", "oncompletetarget", gameObject, "oncompleteparams", _oldad, "oncomplete", "turnoff")); 
	}

	public void turnoff(GameObject _ad)
	{
		limiter = false;
		_ad.SetActive (false);
	}

	public void closeButton()
	{
		iTween.MoveTo (NewsContainer, iTween.Hash ("y", 20, "easetype", "easeInBack", "time", 0.5, "oncompletetarget", gameObject, "oncomplete", "disable"));
	}

	void disable()
	{
		if (OnClosed != null) 
		{
			OnClosed ();
		}
		gameObject.SetActive (false);
	}
}
