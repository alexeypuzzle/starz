﻿using UnityEngine;
using UnityEngine.UI;

public class menuAd : MonoBehaviour {

	public string AppstoreURL;
	public string PlaystoreURL;
	string url;
	
	void Awake()
	{
		#if UNITY_IOS
		url = AppstoreURL;
		#elif UNITY_ANDROID
		url = PlaystoreURL;	
		#endif
		GetComponent<Button>().onClick.AddListener(OnAdClicked);
	}

	public void OnAdClicked()
	{
		if (!string.IsNullOrEmpty(url))
		{
			Application.OpenURL(url);
		}
		else
		{
			Application.OpenURL("http://viker.co.uk");
		}
	}
}
